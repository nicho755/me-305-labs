"""@file    ME405_Lab0x04_main.py

@brief      Measures internal microcontroller temperatures and saves it to a CSV
@details    This lab uses I2C communication between a master device (Nucleo) and
            a slave device (mcp9808 temperature sensor) to determine the ambient
            temperature of a room. A class constructed in a separate file is 
            instantiated in this file by using the initialized Nucelo as one
            argument and the sensors address on the I2C bus as the other argument.
            The final CSV is composed of four columns: the synchronized time
            per temperature measurement, the sensor ambient temp in Celsius, the
            sensor ambient temp in Fahrenheit, and the internal Nucleo temp.
            
            <B>Source code (main file):</B> https://bitbucket.org/nicho755/me-305-labs/src/master/ME405_Lab0x04_HotorNot/ME405_Lab0x04_main.py
            <B>Source code (main file):</B> https://bitbucket.org/nicho755/me-305-labs/src/master/ME405_Lab0x04_HotorNot/mcp9808.py   
            
@image      html Nucleo_Temp.jpg
               
@author     Nicholas Greco
@date       5/13/2021
"""
import pyb
from pyb import I2C
from mcp9808 import mcp9808
import utime
        
# Building in a user input to start data colleciton
if __name__ == '__main__':  
    ## @brief The name of the file we will write to
    fileName = 'Temps.csv'
    
    # i2c = pyb.I2C.init(I2C.MASTER, baudrate = 400000, gencall=False, dma=False)
    
    ## @brief Initializes the I2C object and sets the object to Master mode
    i2c_obj = pyb.I2C(1, I2C.MASTER)
    
    ## @brief Specifies the default sensor address on the bus
    sensAddr = 0x18
    
    ## @brief Instantiates the temperature sensor 
    tempSens = mcp9808(i2c_obj, sensAddr)

    t_lastMsr = utime.ticks_ms()
    ## @brief Sets the ADC address from which we access the Nucleo core temp measurement 
    adc_obj = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
    adc_TempMsr = adc_obj.read_core_vref()
    t_sample = 60000 # will need to be 60000 eventually
    while True:
        try:
        # When a difference between the last measurement time and the current time
        # reaches the desired sample period, the condition of the "if" statement
        # will be met, and the Nucleo temperature will be read.
        
            if utime.ticks_diff(utime.ticks_ms(), t_lastMsr) >= t_sample:
                crntTemp = tempSens.celsius()
                # tempRdg_Sns.append(crntTemp)
            
                print(crntTemp)
                # @brief Stores the core temperature of the Nucelo
                adc_TempMsr = adc_obj.read_core_temp()
                print(adc_TempMsr)
                # resets the last measurement time so that data is consistently sampled
                # at the period set by t_sample
                t_lastMsr = utime.ticks_ms()
            # The means of opening a file and appending data to the CSV file 
                with open (fileName, "a") as TmprMsr_file:
                    TmprMsr_file.write("{:},{:},{:}\r\n".format(t_lastMsr, adc_TempMsr, crntTemp))
            
            # TmprMsr_file.write("Time of Collection [s], Internal MCU Temperature [C], Sensor Temperature [C]\r\n")

        except KeyboardInterrupt:
            print ('Ctrl+C detected')
            TmprMsr_file.close() 
            break
    
    print("The file has by now automatically been closed." 
          "Be excellent to each other... and party on dudes!")
            

    
    # main file, read mCU core temp, sensor temp I2C
    # reading from each data point 
    # button to turn on and off data collection, LED on,
    # button on LED on, button off LED off
    
    
    # 2 bites over I2C 2 byetes: 16 bits for temp data