# -*- coding: utf-8 -*-
"""
Created on Thu May  6 13:07:28 2021

@author: nicho
"""

# pg. 16-17, 24-25 as a useful reference
import pyb

class mcp9808:
    
    # Specifying class variable tempC
    tempC = 0
    # Set up the Temperature Array
    tempReg = 0x05
    # Set up the mfgID to be used in the check method
    mfgID = 0x06
    
    def __init__(self, i2c_obj, sensAddr):
        # give a reference to an already created I2C object
        # give the address of the MCP9808 on the I2C bus
        self.i2c_obj = i2c_obj
        self.sensAddr = sensAddr
        # self.data = bytearray(2) b/c data is an array defined consistently for this sensor
        # Byte array organized as [MSB, LSB] => see pg.17 of the documentation
        self.data = bytearray(2) 
        
    def check(self):
        # verifies that the sensor is attached at the appropriate bus address
        # verification is done by checking current sensor bus address is 
        # what we'd expect from the manufacturer ID register
        
        # Get the appropriate address from pg 27, and check this against 
        # the self.sensAddr variable in the constructor
        self.data = self.i2c_obj.mem_read(2, self.sensAddr, self.mfgID) 
        
        # in bytearray, is the MSB first or LSB first? => (MSB, LSB)
        # Creating a 16 bit number composed of the contents of the bytearray
        # stored in the data variable. By left shifting the MSB 8 spots (thus
        # making room for the LSB) and "or-ing" the MSB with the LSB, we get
        # the 16 bit number corresponsing to the value of the manufacturing ID
        if (self.data[0]<<8|self.data[1]) == 0x0054: # data in the register 
            # think of the manufacturer ID (0x06) as a variable, and the
            # value assigned to the manuf. ID (0x54) as the variable's value
            # i.o.w. 0x06 = 0x54
            print("Welcome to your mcp9808")
            return True
        else:
            print("An incorrect address has been entered")
            return False

    def celsius(self):
               # Method's purpose: To return temperature values in degress celsius
        # Eliminate unnecessary bits, set the sign bit then read from temperature sensor
        # Receives the MSB from the bytearray
        # pyb.I2C.mem_read() reads the data from tempReg and puts it into the bytearray
        self.data = self.i2c_obj.mem_read(2, self.sensAddr, self.tempReg)
        
        # self.data[0] = pyb.I2C.mem_read(1, self.sensAddr, self.tempReg)
        # # Receives the LSB from the bytearray
        # self.data[1] = pyb.I2C.mem_read(1, self.sensAddr, self.tempReg)
        
        # self.data = pyb.I2C.mem_read(2, self.sensAddr, self.tempReg)
        
        # MSB: The Sign bit location is specified as the hexidecimal "10"
        # To specify the hexidecmal "10" in binary, type:
        # 0x00010000 b/c hexadecimal reads this as 0001 + 0000
        # aka "1" and "0" or "10" => 0x10
      
        MSB = self.data[0]
        LSB = self.data[1]
        # Clears flag bits
        MSB = MSB & 0x1F
        # If the Upper Byte Value is "and-ed" with the sign bit value and 
        # returns the sign bit value, then the sign bit is set to true
        if MSB & 0x10 == 0x10:
            # By using the Ampersand (&) with the binary equivalen, we can ensure the 
            # sign bit is set to zero
            MSB = MSB & 0x0F
            # This temp variable is something I am interpreting from the bit values
            # returned from the Ambient Temperature Register
            self.tempC = 256 - (MSB*16 + LSB*(1/16))
        else:
            self.tempC = (MSB*16 + LSB*(1/16))
        
        return(self.tempC)
        
        # Example 5-1 on page 25
        # ALlows us to access upper bits in order to read the bits
        # and perform appropriate calculations that allow us to turn
        # bit data into temperature data
        
        # memory address for this function: 0x00000101 aka 0x05
        
        
    def fahrenheit(self):
        # Method's purpose: To return temperature values in degrees fahrenheit
        # can i use the same protocal as above, and simply employ
        # the conversion equation from celsius to fahrenheit?
        # Conversion: (degC * (9/5)) + 32 = degF
        
        # Calling the celsius function so that the fahrenheit funciton never 
        # returns an empty array 
        tempF = (self.celsius()*(9/5)) + 32
        return tempF