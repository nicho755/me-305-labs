# -*- coding: utf-8 -*-
"""@file    Lab0x03_SimonSays.py

@brief      An interactive, pattern-matching game using Morse Code sequences
@details    Implements a finite state machine, shown below,...
            to present the user with a series of progressively more difficult
            patterns to match. The patterns are based on Morse Code sequences
            for the 9 most common letters of the alphabet. From the initial 
            state, called the GameMaster, a random number is selected, which is
            then used as an index to access a list of Morse Code functions that
            display, then ask the user to match the pattern they just saw.
            Depending upon the accuracy of their response, the user is then 
            sent to the next level or sent to an error state to try their luck
            again.
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/Lab2/
            
@author     Nicholas Greco
@date       2/18/2021
"""


import math
import utime
import pyb
import random 

# Morse Code website: https://web.northeastern.edu/stemout/morse-code

# The two functions below are callback functions. There are two of them b/c
# measuring the time between them will be important when evaluating if the
# user entered the correct pattern.

# A dash in length is defined as 750 ms; a dot in length is defined as 250 ms,
# as is the lenght of a space i.e. len_dash = 3*len_dot & len_dot = len_space


BFlag_Fall = False
BFlag_Rise = True

GameMaster = 1
Disp_Trans = 2
Interp_Trans = 3
ErrorState = 4
VictoryState = 5
ExitState = 6
# The above variables are true global variables because every component of the
# code below has access to them

# def List_Disp_Trans(LetterList):
    
#     for Letter in LetterList:
#         Disp_Trans(Letter)
    
# def List_Interp_Trans(LetterList):
    
#     for Letter in LetterList:
#         state = Interp_Trans(Letter)
#         if state == ErrorState:
#             return state
#     return state

def Disp_Trans(Letter):
    AlphabetLegend = [disp_A, disp_C]
                     #disp_E, I_func, N_func, O_func, R_func, S_func, T_func]
    AlphabetLegend[Letter]()    

def Interp_Trans(Letter):

    AlphabetLegend = [interp_A,  interp_C]
                    # interp_E, I_func, N_func, O_func, R_func, S_func, T_func]
    state = AlphabetLegend[Letter]() # + AlphabetLegend[LetterList(1)]()
    return state

def disp_A():
    Disp_Dot()
    Disp_Space()
    Disp_Dash()
    
def disp_C():
    Disp_Dash()
    Disp_Space()
    Disp_Dot()
    Disp_Space()
    Disp_Dash()
    Disp_Space()
    Disp_Dot()

def interp_A():
    state = GameMaster
    # The state is set to that of the GameMaster and the  program is designed 
    # with if statements that will send the user to the error state unless the
    # program in the called function is matched correctly.
    print('Scooby-Dot')
    notInTol = False
    
    if  MC_Dot() == notInTol:
        print('User Incorrect Dot')
        state = ErrorState
        return state

    elif  MC_Space() == notInTol:
        print('User Incorrect Space')
        state = ErrorState
        return state
    
    elif  MC_Dash() == notInTol:
        print('User Incorrect Dash')
        state = ErrorState
        return state
    
    print('yay')
    return state # default state is GameMaster
    

def interp_C():
    state = GameMaster
    # The state is set to that of the GameMaster and the  program is designed 
    # with if statements that will send the user to the error state unless the
    # program in the called function is matched correctly.
    
    notInTol = False
    if  MC_Dash() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dash() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state

    print('yay')
    return state # default state is GameMaster

def interp_E():
    state = GameMaster
    
    notInTol = False    
    
    if  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    print('yay')
    return state # default state is GameMaster

def I_func():
    state = GameMaster
    # The state is set to that of the GameMaster and the  program is designed 
    # with if statements that will send the user to the error state unless the
    # program in the called function is matched correctly.
    
    notInTol = False    
    if  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    print('yay')
    return state # default state is GameMaster

def N_func():
    # dash dot
    state = GameMaster
    
    notInTol = False
    if  MC_Dash() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    print('yay')
    return state # default state is GameMaster

def O_func():
    # dash dash dash
    state = GameMaster
    
    notInTol = False
    if  MC_Dash() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dash() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dash() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    print('yay')
    return state # default state is GameMaster

def R_func():
    # Dot Dash Dot
    state = GameMaster
    
    notInTol = False    
    if  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dash() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    print('yay')
    return state # default state is GameMaster

def S_func():
    # dot dot dot
    state = GameMaster
    
    notInTol = False    
    if  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Space() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    elif  MC_Dot() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    print('yay')
    return state # default state is GameMaster

def T_func():
    # dash
    state = GameMaster
    
    notInTol = False
    if  MC_Dash() == notInTol:
        print('User Incorrect')
        state = ErrorState
        return state
    
    print('yay')
    return state # default state is GameMaster

def MC_Dash():
    # MC_Dash returns either True or False
    isInTol = False
    # this variable is always set to False, and only under certain conditions
    # fulfilled by the user will this "check" variable be set to true, at
    # which point the program will know the user has met the pattern conditions
    # or has not met them, and will return the appropriate value.'

    if BFlag_Fall == True:
        t2ch1.pulse_width_percent(100)
        t_start = utime.ticks_ms()
        print(t_start)
        t_dash = 0
        while t_dash < t_start + 1250:
            t_dash = utime.ticks_ms()
            print(t_dash)
            if BFlag_Rise == True:
                t2ch1.pulse_width_percent(0)
                if abs((t_start + 250) - t_dash) < 1000:
                    # the user is correct anywhere between 250 & 1250 ms
                    isInTol = True
                    return isInTol
                else:
                    return isInTol
        t2ch1.pulse_width_percent(0)
        return isInTol
    t2ch1.pulse_width_percent(0)
    return isInTol
        # upon exiting the loop, you have automatically exceeded the allowable
        # time, as the dash is set to 750 ms (menaing the above code gives you
        # a +/- 50 ms tolerance)

def MC_Dot():
    isInTol = False
    
    if BFlag_Fall == True:
        t2ch1.pulse_width_percent(100)
        t_start = utime.ticks_ms()
        t_dot = 0
        while t_dot < t_start + 500:
            t_dot = utime.ticks_ms()
            if BFlag_Rise == True:
                t2ch1.pulse_width_percent(0)
                if abs((t_start + 100) - t_dot) < 400:
                    # The user input is correct anywhere between 100 & 500 ms
                    isInTol = True
                    return isInTol
                else:
                    return isInTol
        t2ch1.pulse_width_percent(0)
        return isInTol
    t2ch1.pulse_width_percent(0)
    return isInTol
        # upon exiting the loop, you have automatically exceeded the allowable
        # time, as the dash is set to 750 ms (menaing the above code gives you
        # a +/- 50 ms tolerance)

def MC_Space():
    isInTol = False
    if BFlag_Rise == True:
        t_start = utime.ticks_ms()
        print(t_start)
        t_space = 0
        while t_space < t_start + 500:
            t_space = utime.ticks_ms()
            print(t_space)
            if BFlag_Fall == True:
                if abs((t_start + 100) - t_space) < 400:
                    isInTol = True
                    return isInTol
                else:
                    return isInTol
        return isInTol
    return isInTol

def Disp_Dash():
    time1 = utime.ticks_ms()
    
    while utime.ticks_ms() - time1 < 750:
        t2ch1.pulse_width_percent(100)
    t2ch1.pulse_width_percent(0)       
    
def Disp_Dot():
    time1 = utime.ticks_ms()
    
    while utime.ticks_ms() - time1 < 250:
        t2ch1.pulse_width_percent(100)
    t2ch1.pulse_width_percent(0)       
    
def Disp_Space():
    time1 = utime.ticks_ms()
    
    while utime.ticks_ms() - time1 < 250:
        t2ch1.pulse_width_percent(0)  


        # upon exiting the loop, you have automatically exceeded the allowable
        # time, as the dash is set to 750 ms (menaing the above code gives you
        # a +/- 50 ms tolerance)



def ButtonPresser(IRQ_src):
    '''@brief The callback func. assoc. with the falling edge caused by the button push
       @param IRQ_src the interrupt cmd that activates when the button is pushed
       @return ButtonFlag becomes True
    '''

    global BFlag_Fall
    BFlag_Fall = not BFlag_Fall

    global BFlag_Rise
    BFlag_Rise = not BFlag_Rise

    # The two callback functions are defined opposite of one another in order
    # to achieve an effect akin to pushing one button down and causing another
    # button to rise (sort of like bumps on the heads of cartoon characters
    # a la Tom & Jerry)

if __name__ == '__main__':

    print("Welcome. Press the Button to begin Simon Says.")

    if BFlag_Fall == False:
        print('Falling Flag is False')

    if BFlag_Rise == True:
        print('Rising Flag is True')

    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

    ButtonIntr =  pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                pull=pyb.Pin.PULL_NONE, callback=ButtonPresser)
    # Check documentation to get a Falling & Rising Edge
    state = GameMaster
    if state == GameMaster:
        print('Initialized: Shall we play a game?')
    # Set state to 0

    t2ch1.pulse_width_percent(0)
    
    while(True):
        try:
           #MC_Dot()
          #Letter = 0 - not needed because varibales defined in an if 
           #statement are NOT local to the if-statement
            if state == GameMaster:
                #Letter = random.randint(0,8)
                Letter = 0 # for testing purposes; should be A
                # LetterList = []
                state = Disp_Trans
                shldDisp = True
                
            elif state == Disp_Trans:
                if shldDisp == True:
                    Disp_Trans(Letter)
                    print('Now enter your input')
                    shldDisp = False
                    
                if BFlag_Fall == True:
                    state = Interp_Trans
                    shldDisp = True
            
            # if state == GameMaster:
            #     Letter = random.randint(0,8)
            #     LetterList = []
            #     if len(LetterList) <= 3:
            #         LetterList.append(Letter)
            #     else:
            #         state = VictoryState
            #     state = Disp_Trans
            #     shldDisp = True
            
            # elif state == Disp_Trans:
            #     if shldDisp == True:
            #         Disp_Trans(LetterList)
            #         print('Now enter your input')
            #         shldDisp = False
            
            
            elif state == Interp_Trans:
                state = Interp_Trans(Letter)
            
            elif state == ErrorState:
                print('That was not quite the pattern we expected.'
                      'Would you like to play again? Press once for yes,'
                      'or enter nothing if you wish to stop playing.')
                
                # LossList = []
                state = ExitState
                t_init_ES = utime.ticks_ms()
                t_error = 0
                while t_error < t_init_ES + 5000:
                    t_error = utime.ticks_ms()
                    if BFlag_Fall == True:
                        t2ch1.pulse_width_percent(100)
                        print('Returning you to the GameMaster for more'
                              'pattern-matching fun!')
                        state = GameMaster

            
            elif state == VictoryState:
                print('Well Done! You have exceeded all expectations!.'
                      'Would you like to play again? Press once for yes,'
                      'or enter nothing if you wish to stop playing.')
               
               # WinList = []
                state = ExitState
                t_init_VS = utime.ticks_ms()
                t_vic = 0
                while t_vic < t_init_VS + 5000:
                    t_vic = utime.ticks_ms()
                    if BFlag_Fall == True:
                        t2ch1.pulse_width_percent(100)
                        print('Returning you to the GameMaster for more'
                              'pattern-matching fun!')
                        state = GameMaster
            
            elif state == ExitState:
                print('No button press detected; exiting the game now.')
                # display the win-loss record
                print('Thank you for playing. See ya real soon!')
                break

        except KeyboardInterrupt:
            # THis except block catches Ctrl-C from the keyboard
            # and only broke the while(True) loop when desired
            t2ch1.pulse_width_percent(0)
            break

    #Program De-Initialization goes here
    t2ch1.pulse_width_percent(0)
    print ('Sun is setting high')
                                    
 

            #     if BFlag_Fall == True:
            #         state = 1
            #         # BFlag_Fall = False
            #         # BFlag_Rise = True
            #         time1 = utime.ticks_ms()
            #         print(time1)
            #         # originially for troubleshooting purposes,
            #         # now a comment of a good practice I need to carry forward
            #         print('Entering State 1, The Game Begins, from the'
            #               ' Initialization State.')

            # elif state == 1:
            #     # This code will run the letter "K" in Morse Code, with each
            #     # unit of the code (spaces and dots) being displayed in 200 ms
            #     # intervals.

            #     if utime.ticks_diff(utime.ticks_ms(), time1) == 0:
            #         t2ch1.pulse_width_percent(100)
            #         print('START')
            #     elif utime.ticks_diff(utime.ticks_ms(), time1) < 500:
            #         t2ch1.pulse_width_percent(100)
            #         print('ON')
            #     elif utime.ticks_diff(utime.ticks_ms(), time1) < 1000:
            #         t2ch1.pulse_width_percent(0)
            #         print('OFF')
            #     elif utime.ticks_diff(utime.ticks_ms(), time1) < 1500:
            #         t2ch1.pulse_width_percent(100)
            #         print('ON')
            #     elif utime.ticks_diff(utime.ticks_ms(), time1) < 2000:
            #         t2ch1.pulse_width_percent(0)
            #         print('OFF')
            #     elif utime.ticks_diff(utime.ticks_ms(), time1) >= 2000:
            #         t2ch1.pulse_width_percent(0)
            #         state = 2
            #         print('STOP')
            #         # break

            #     # when running the function, I received the error "t0_fall
            #     # not defined". I believe this error to be the result of the
            #     # program not being interrupted to allow for a user input,
            #     # meaning that t0_fall is not defined in the while loop fall

            #     # Keeping the FSM in the same state until the button is pressed.
            #     # I need to break the if-elif statements below into separate
            #     # states so that each leads to the other

            # elif state == 2:
            #     MC_Dot()
            #     print('Yo-Ho, Yo-Ho')
            # elif state == 3:
            #     MC_Space()
            #     print('A Pirates Life For Me')
            # elif state == 4:
            #     MC_Dot()
            #     print('We pillage and plunder')
            # elif state == 5:
            #     print('Congratulations')
            #     print("Now, re-enter the pattern to the best of your abilities.")
            #     if BFlag_Fall == True:
            #         # I want to measure the time the button was pressed for
            #         # I want to store the button presses in the order they were
            #         # keyed into the program
            #         t0_fall = utime.ticks_ms()
            #         # records the single instance at which the button is pressed
            #         state = 3
            #         print('Yo-Ho, Yo-Ho')

            # elif state == 3:
            #     if BFlag_Rise == True:
            #         t0_rise = utime.ticks_ms()
            #         # records the single instance at which the button is released
            #         state = 4
            #         print('A Pirates Life For Me')


            # elif state == 4:
            #     del_T0_H = t0_rise - t0_fall
            #     print(del_T0_H)

            #     if del_T0_H > 400 and del_T0_H < 600:
            #         print('Congratulations')
            #         # BACK to STATE 1? (and state 1 will have to update?)
            #     else:
            #         print('The pattern has not been matched')
            #         # state = 5; this will have to be a state that is either
            #         # larger or smaller than the other states within the FSM




