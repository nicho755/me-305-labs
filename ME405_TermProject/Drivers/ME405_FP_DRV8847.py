'''
@file ME405_FP_DRV8847.py
@brief This module contains several classes used to interface with the DRV8847 motor controller board.
@details <B> source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Drivers/ME405_FP_DRV8847.py
@author Eliot Briefer
@date 6/11/2021
'''
import pyb, utime

class DRV8847:
    '''
    @brief The DRV8847 class is used to enable and dissable the DRV8847 motor controller board as well as handle fault detection.
    @details Note that the DRV8847 class does not control either motor conected to the board. Individual motors are controlled by
    the DRV8847_channel class.
    '''
    def __init__(self,nSLEEP_pin, nFAULT_pin, maxFaultSep = 500):
        '''
        @brief A method to initialize a DRV8847 object
        @param nSLEEP_pin A pyb.Pin object that is connected to the nSLEEP_pin on the DRV8847 board.
        @param nFAULT_pin A pyb.Pin object that is connected to the nFAULT_pin on the DRV8847 board.
        @param maxFaultSep The maximum sepperation between board faults to trigger a fault and disable the motors (in ms)
        '''
        ##@brief The pyb.Pin object that enables and diables the motor.
        self.nSLEEP = nSLEEP_pin
        ##@brief The pyb.Timer object used to generate PWM for the motor.
        self.nFault = nFAULT_pin
        self.faultInt = pyb.ExtInt(self.nFault, mode=pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback=self.fault_CB)
        ##@brief Wether or not the DRV8847 object is in a fault condition. The motors will not enable until DRV8847.clear_fault is called.
        self.fault = False
        self.maxFaultSep = maxFaultSep #the maximum time between fault_cb interupts to still trigger a fault
        self.lastFaultTime = utime.ticks_ms()
        
    
    def enable(self):
        '''
        @brief A method to enable the motor by pulling the sleep pin high.
        '''
        if not self.fault:
            self.nSLEEP.value(1)
            #print('Enabling motor')
    
    def disable(self):
        '''
        @brief A method to disable the motor by pulling the sleep pin low.
        '''
        self.nSLEEP.value(0)
        #print('Disabling motor')
    
    def fault_CB(self, IRQ_src):
        '''
        @brief An interrupt used to dissable the motors and enter a fault state if the DRV8847 board signals a fault.
        @details Due to the sensititivy of the fault pin on the DRV8847 board, a fault
        is only triggered if two faults occur within DRV8847.maxFaultSep milliseconds of eachother
        (default is 500 ms).
        '''
        now = utime.ticks_ms()
        print('FAULT_CB TRIGGERED!')
        if utime.ticks_diff(now, self.lastFaultTime) < self.maxFaultSep:
            print('Fault criteria met, disabling')
            self.fault = True
            self.disable()
        self.lastFaultTime = now
    
    def clear_fault(self):
        '''
        @brief A method used to clear a fault so that the motors can be enabled.
        @details The fault will only clear if the DRV8847 board is not currently in a fault condition.
        @return Wether or not the fault was sucsesfully cleared. True = cleared, False = not cleared.
        '''
        if self.nFault.value() == 1:
            self.fault = False
            print('Fault cleared')
            return True
        else:
            print('Cannot clear fault')
            return False
    
    def channel(self, INx_pin, INx_ch, INy_pin, INy_ch, INxy_timer):
        '''
        @brief A method to create a DRV8847_channel object for controlling individual motors.
        @param INx_pin A pyb.Pin object connected to one lead of the motor via the DRV8847 board.
        @param INx_ch The timer channel corresponding to INx_pin (integer).
        @param INy_pin A pyb.Pin object connected to the other lead of the motor via the DRV8847 board.
        @param INy_ch The timer channel corresponding to INy_pin (integer).
        @param INxy_timer A pyb.Timer object to use for PWM on both the motor leads.
        @return A DRV8847_channel object to set the duty cycle of the motor.
        '''
        return DRV8847_channel(INx_pin, INx_ch, INy_pin, INy_ch, INxy_timer)
    
class DRV8847_channel:
    '''
    @brief The DRV8847_channel class is used to set the duty cycle of an individual motor connected to the DRV8847 board.
    @details DRV8847_channel objects should only be used in conjunction with the DRV8847 class and should be instantiated by
    calling DRV8847.channel() instead of DRV8847_channel()
    '''
    def __init__(self, INx_pin, INx_ch, INy_pin, INy_ch, INxy_timer):
        '''
        @brief A method to initialize a DRV8847_channel object.
        @details Do not call this method directly using DRV8847_channel(), instead use DRV8847.channel() from an already
        initialized DRV8847 object.
        '''
        ##@brief The pyb.Timer object used to generate PWM for the motor.
        self.timer = INxy_timer
        ##@brief The channel of the PWM timer corresponding to the first motor lead.
        self.IN1 = self.timer.channel(INx_ch, pyb.Timer.PWM, pin=INx_pin)
        ##@brief The channel of the PWM timer corresponding to the second motor lead.
        self.IN2 = self.timer.channel(INy_ch, pyb.Timer.PWM, pin=INy_pin)
        #print('Creating a motor driver')
    
    def set_level(self, level):
        '''
        @brief A method to set the duty cycle of the motor.
        @param duty The duty cycle to set the motor to, from -100 to 100.
        '''
        if level >= 0:
            self.IN1.pulse_width_percent(level)
            self.IN2.pulse_width_percent(0)
            #print('Set duty cycle to: ' + str(duty))
        else:
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(-1* level)
            #print('Set duty cycle to: ' + str(duty))
    
if __name__ == "__main__":
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, mode=pyb.Pin.OUT_PP, value=1);
    pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2, mode=pyb.Pin.IN)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5);
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0);
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1);
    tim3 = pyb.Timer(3, freq = 20000);
    motorBoard = DRV8847(pin_nSLEEP, pin_nFAULT)
    motor1 = motorBoard.channel(pin_IN1, 1, pin_IN2, 2, tim3)
    motor2 = motorBoard.channel(pin_IN3, 3, pin_IN4, 4, tim3)
    motor2.set_level(50)
    pyb.delay(5000)
    motorBoard.disable()