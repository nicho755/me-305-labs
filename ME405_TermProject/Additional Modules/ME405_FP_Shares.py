"""@file    ME405_FP_Shares.py

@brief      Shares and Queues of the term projects defined here
@details    Using the provided task_share.py file, shares and queues are instantiated here
            to be shared with multiple different tasks. These shares and queues
            are separated by the tasks that are puttin values into said objects.
            \n
            <B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Additional%20Modules/ME405_FP_Shares.py
@author     Eliot Briefer & Nicholas Greco
@date       6/11/2021
"""

import task_share as ts

#STATE VARIABLES:   
#Shares set by touch pannel, read by controller:

## @brief The x-position SHARE set by the TOUCH PANEL
xShare = ts.Share('f', name = "Ball X Position")
## @brief The x-velocity SHARE set by the TOUCH PANEL
xDotShare = ts.Share('f', name = "Ball X Velocity")
## @brief The y-position SHARE set by the TOUCH PANEL
yShare = ts.Share('f', name = "Ball Y Position")
## @brief The y-velocity SHARE set by the TOUCH PANEL
yDotShare = ts.Share('f', name = "Ball Y Velocity")

#Queues set by touch pannel, read by controller:
    
## @brief The x-position QUEUE set by the TOUCH PANEL
xPos_Q = ts.Queue ('f', 50, thread_protect = False, overwrite = True, 
                       name = "xPos_Q")
## @brief The y-position QUEUE set by the TOUCH PANEL
yPos_Q = ts.Queue ('f', 50, thread_protect = False, overwrite = True,
                       name = "yPos_Q")
## @brief The x-velocity QUEUE set by the TOUCH PANEL
xDot_Q = ts.Queue ('f', 50, thread_protect = False, overwrite = True,
                       name = "xDot_Q")
## @brief The y-velocity QUEUE set by the TOUCH PANEL
yDot_Q = ts.Queue ('f', 50, thread_protect = False, overwrite = True,
                       name = "yDot_Q")



#Shares set by encoders, read by controller:
    
## @brief The SHARE set by the ENCODER for angular displacement about the X-AXIS
thetaXShare = ts.Share('f', name = "Plate X angle")
## @brief The SHARE set by the ENCODER for angular velocity about the X-AXIS
thetaXDotShare = ts.Share('f', name = "Plate X Angular Velocity")
## @brief The SHARE set by the ENCODER for angular displacement about the Y-AXIS
thetaYShare = ts.Share('f', name = "Plate Y angle")
## @brief The SHARE set by the ENCODER for angular velocity about the Y-AXIS
thetaYDotShare = ts.Share('f', name = "Plate Y Angular Velocity")

#Queues set by encoders, read by controller:
    
## @brief The QUEUE set by the ENCODER for angular displacement about the X-AXIS
thetaXQueue = ts.Queue ('f', 50, thread_protect = False, overwrite = True,
                       name = "thX_Q")
## @brief The QUEUE set by the ENCODER for angular velocity about the X-AXIS
thetaXDotQueue = ts.Queue ('f', 50, thread_protect = False, overwrite = True,
                       name = "omX_Q")
## @brief The SHARE set by the ENCODER for angular displacement about the Y-AXIS
thetaYQueue = ts.Queue ('f', 50, thread_protect = False, overwrite = True,
                       name = "thY_Q")
## @brief The SHARE set by the ENCODER for angular velocity about the Y-AXIS
thetaYDotQueue = ts.Queue ('f', 50, thread_protect = False, overwrite = True,
                       name = "omY_Q")
## @brief The SHARE set by the ENCODER to monitor the TIME that corresponds to the colleciton of values put into queues
encTimeQueue = ts.Queue('f', 50, thread_protect = False, overwrite = True,
                       name = "Encoder Time Queue")

#INPUT VARIABLES:
#Set by controller, read by motor task:

## @brief The SHARE set by the CONTROLLER for the DUTY CYCLE to be delivered to the X-Motor
xDutyCycleShare = ts.Share('f', name = "X Motor Duty Cycle")
## @brief The SHARE set by the CONTROLLER for the DUTY CYCLE to be delivered to the Y-Motor
yDutyCycleShare = ts.Share('f', name = "Y Motor Duty Cycle")

## @brief The Boolean value set in the USER TASK that indicates if data collection should begin or stop
dataClct = ts.Share('i', name ="Data collect")

#OTHER SHARED DATA:

## @brief The Boolean value set in the USER TASK that tells the platform to begin or stop balancing
enabledShare = ts.Share('i', name = "Enabled") # 1 = enabled, 0 = disabled
## @brief The Boolean value set in the USER TASK that allows the user to clear a motor fault
resetShare = ts.Share('i', name = "Reset Motor Driver Error") # 1 = Reset motor drivers, 0 = don't
## @brief The Boolean value set in the USER TASK that exits the program and stops motor operation
programExitShare = ts.Share('i', name = "Program Exit Share")
## @brief The sample period (ms) at which the Touch Panel, Controller, and Encoder all run
t_sample = ts.Share('f', name = "Sample Time")
smplPer = 20
t_sample.put(smplPer)