"""@file    ME405_FP_TouchPanelTask.py

@brief      Instantiates touch panel with proper components and filters the motion output
@details    Specifies appropriate pins for the x-positive, y-positive, x-negative,
            and y-negative directions. Filters the readings of the balls
            position and velocity data using an alpha-beta filter.
            \n
            <B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Tasks/ME405_FP_TouchPanelTask.py
@author     Nicholas Greco
@date       6/11/2021
"""
import pyb
import utime
from ME405_FP_ResTP import ResTP
from ME405_FP_Shares import xShare, xDotShare, yShare, yDotShare
from ME405_FP_Shares import xPos_Q, yPos_Q, xDot_Q, yDot_Q, t_sample, enabledShare, dataClct
from ME405_FP_ABFilter import ABFilter

Clear_State = 0
Wait_State = 1
Collect_State = 2
Filter_State = 3

def touchPanelTaskFun ():
    ''' @brief Instantiates the touch panel and sets up the data to be filtered
        @details The specific pins are set-up so that ym, xm, yp, and xp can provide
                 full range of motion data for the 2D  touch panel plane. The touch
                 panel is then instantiated with the appropriate pins, then
                 appropriate alpha & beta values are selected. 
    '''
    pinA0 = pyb.Pin(pyb.Pin.cpu.A0) # ym of the touch panel
    pinA1 = pyb.Pin(pyb.Pin.cpu.A1) # xm of the touch panel
    pinA6 = pyb.Pin(pyb.Pin.cpu.A6) # yp of the touch panel
    pinA7 = pyb.Pin(pyb.Pin.cpu.A7) # xp of the touch panel

    tchPanel = ResTP(pinA0, pinA1, pinA6, pinA7)
    AB_motionX = ABFilter(0.85, 0.005, t_not = utime.ticks_us(), useUtime = True) # Instantiate AB Filter
    AB_motionY = ABFilter(0.85, 0.005, t_not = utime.ticks_us(), useUtime = True) # Instantiate AB Filter
        
    state = Wait_State
    while True:
        if enabledShare.get() == 1:
            startTime = utime.ticks_us()
            scanRdg = tchPanel.runScan()
            whereTouch = []
            whereTouch.clear()
            if scanRdg[2] == True:
                # print("Entering Collect State")
                whereTouch = [utime.ticks_diff(utime.ticks_us(), startTime), scanRdg[0], scanRdg[1]]
                (x, v_x) = AB_motionX.update(whereTouch[1], utime.ticks_us()) 
                (y, v_y) = AB_motionY.update(whereTouch[2], utime.ticks_us()) 
                # Motion Data in the "x" direction
                xShare.put(x)
                # print(xShare.get())
                xDotShare.put(v_x)
                # print(xDotShare.get())
                # Motion Data in the y" direction
                yShare.put(y)
                yDotShare.put(v_y)          
                # print("Naive_X {}, Naive_Y {}, FIltered_X {}, Filtered_Y{}".format(whereTouch[1], whereTouch[2], xShare.get(), yShare.get()))  
                # print("Filtered_xDot {}, Filtered_yDot {}".format(xDotShare.get(), yDotShare.get()))
                # print("Filtered X-Scan: {}, Filtered Y-Scan: {}".format(xShare.get(), yShare.get()))  
                # Assuming that all 4 queues fill at the same rate as they are the same length
                if (not xPos_Q.full()) and dataClct.get() == 1:
                    xPos_Q.put(x)    
                    #print(xPos_Q.any())
                    #print(xPos_Q.num_in())
                    yPos_Q.put(y)
                    xDot_Q.put(v_x)
                    yDot_Q.put(v_y)
            
            #     state = Filter_State
            # print("Entering Clear State")
            # if state == Clear_State:
            #     whereTouch.clear()
            #     print("Entering Wait State")
            #     state = Wait_State
            #     print(state)
            # elif state == Wait_State:
            #     print("Waiting for contact")
            #     if scanRdg[2] == True:
            #         state = Collect_State
            #         print("Entering Collect State")
            # elif state == Collect_State:
            #     whereTouch = [utime.ticks_diff(utime.ticks_us(), startTime), scanRdg[0], scanRdg[1]]
            #     print("Naive X-Scan: {}, Naive Y-Scan: {}".format(whereTouch[0], whereTouch[1]))  
            #     state = Filter_State
            # elif state == Filter_State:
            #     (x, v_x) = AB_motionX.update(whereTouch[1], utime.ticks_us()) 
            #     (y, v_y) = AB_motionY.update(whereTouch[2], utime.ticks_us()) 
            #     # Motion Data in the "x" direction
            #     xShare.put(x)
            #     # print(xShare.get())
            #     xDotShare.put(v_x)
            #     # print(xDotShare.get())
            #     # Motion Data in the y" direction
            #     yShare.put(y)
            #     yDotShare.put(v_y)          
            #     print("Filtered X-Scan: {}, Filtered Y-Scan: {}".format(xShare.get(), yShare.get()))  
            #     # Assuming that all 4 queues fill at the same rate as they are the same length
            #     if not xPos_Q.full():
            #         xPos_Q.put(x)    
            #         #print(xPos_Q.any())
            #         #print(xPos_Q.num_in())
            #         yPos_Q.put(y)
            #         xDot_Q.put(v_x)
            #         yDot_Q.put(v_y)
                
            #     state = Clear_State
        
        yield(0)