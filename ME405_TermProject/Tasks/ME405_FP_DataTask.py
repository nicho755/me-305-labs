"""@file    ME405_FP_DataTask.py

@brief      Writes data from CSV upon command
@details    Imports every state variable queue in both directions of motion and
            stores the corresponding data in a CSV. There is an additional function
            that serves to close the CSV at regular intervals (implementation for this in main.py)
            \n
            <B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Tasks/ME405_FP_DataTask.py
@author     Eliot Briefer & Nicholas Greco
@date       6/11/2021
"""
from ME405_FP_Shares import encTimeQueue, thetaXQueue, thetaXDotQueue, thetaYQueue, thetaYDotQueue, xPos_Q, xDot_Q, yPos_Q, yDot_Q, dataClct
import utime

fileName = 'Platform_Motion.csv'
BalanceData_file = open('Platform_Motion.csv','w')

def dataCollectionTask():
    ''' @brief Collects values of the state vector in the "x" & "y" directions
        @details If the user indicates they want data collection to begin, and
                 this by pressing the "d" command in the User Task, the scheduler 
                 will begin to write data into the specified CSV
    '''
    while True: 
        if dataClct.get() == 1:      
            # encTimeQueue.get()
            # The means of opening a file and appending data to the CSV file 
            BalanceData_file.write("{:},{:},{:},{:},{:},{:},{:},{:},{:}\r\n".format(encTimeQueue.get(), thetaXQueue.get(), thetaXDotQueue.get(), thetaYQueue.get(), thetaYDotQueue.get(), xPos_Q.get(), xDot_Q.get(), yPos_Q.get(), yDot_Q.get()))
        yield(0)

def closeCSV():
    BalanceData_file.close()

                
             
        
                    



