'''
@file ME405_FP_ControllerTask.py
@brief The controller task implements a full state controller to calculate the duty cycles for both motors required to balance the ball.
@details The controller task reads the system state shared by the @ref ME405_FP_EncoderTask.py "encoder task" and 
the @ref ME405_FP_TouchPanelTask.py "touch panel task". Then, it uses two @ref ME405_FP_FSC.FSC "full state contollers"
(one for the x and one for the y axis) to calculate the apropriate duty cycle for each motor to balance the ball.
The duty cycles are then shared with the @ref ME405_FP_MotorTask.py "motor task".
\n
<B>source code:</B> https://bitbucket.org/eliotBriefer/me405_term_project/src/master/Tasks/ME405_FP_ControllerTask.py
@author Eliot Briefer
@Date 6/11/2021
'''

from ME405_FP_FSC import FSC
from ME405_FP_Shares import xShare, xDotShare, yShare, yDotShare, thetaXShare, thetaXDotShare, thetaYShare, thetaYDotShare, xDutyCycleShare, yDutyCycleShare, enabledShare

def controllerTaskFun():
    '''
    @brief A generator function used to calculate the duty cycle of each motor.
    @details controllerTaskFun is formatted as a generator function for use with
    cotask.py and task_share.py.
    '''
    #Initialize x and y controllers:
    ##@brief list of state space x controller gains.
    #@details format is: [x, theta, x_dot, theta_dot]
    xGains = [1.7519*120, 0.6088*250000, 0.4783*120, 0.0528*50000]
    ##@brief list of state space y controller gains.
    #@details [y, theta, y, theta_dot]
    yGains = [1.7519*200, 0.6088*250000, 0.4783*120, 0.0528*50000]
    xController = FSC(4, xGains)
    yController = FSC(4, yGains)
    
    #motor parameters:
    ##@brief motor terminal resistance in ohms
    rt = 1
    ##@brief motor speed constant rad/sec-V
    kv = 693 * (3.14 / 0.5) * (1 / 60)
    ##@brief supply voltage to motor board in V
    vs = 18
    
    #mechanical system parameters:
    ##@brief Reduction ratio from motor to encoder shaft
    n = 4 #reduction ratio (motor rotations: lever arm rotations)
    
    ##@brief conversion factor from torque to duty cycle
    cf = 100 * rt / (4 * kv * vs)
    
    
    while True:
        if enabledShare.get() == 1:
            #Running
            #Calculate desired torque using full state controllers:
            xState = [xShare.get(), thetaYShare.get(), xDotShare.get(), thetaYDotShare.get()]
            yState = [yShare.get(), thetaXShare.get(), yDotShare.get(), thetaXDotShare.get()]
            xTorque = -1 * xController.update(xState) / n
            yTorque = -1 * yController.update(yState) / n
            
            #Convert from torque to duty cycle
            xDutyCycle = cf*xTorque
            yDutyCycle = cf*yTorque
            
            #Update motor duty cycles to send to motor drivers:
            xDutyCycleShare.put(xDutyCycle)
            yDutyCycleShare.put(yDutyCycle)
        yield(0)
        
if __name__ == "__main__":
    #TEST CODE
    import cotask, gc, pyb
    
    #test initilization
    testTask = cotask.Task(controllerTaskFun, name = 'controller_task_test', priority = 1, 
                         period = 1000, profile = True, trace = False)
    cotask.task_list.append (testTask)
    
    #test enable and dissable
    gc.collect ()
    print("should print 'not running!'")
    enabledShare.put(0)
    cotask.task_list.pri_sched() #run task once
    pyb.delay(100) # wait 100 ms
    print("should print 'running!'")
    enabledShare.put(1)
    cotask.task_list.pri_sched() #run task once
    
    
    