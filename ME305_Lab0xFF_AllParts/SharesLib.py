# -*- coding: utf-8 -*-
"""@file    SharesLib.py

@brief      Stores a group of variables shared amongst all tasks
@details    Variables are represented here as potential values and are filled
            in due to the operation of other tasks. These shared values are 
            able to be accessed across all tasks
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/ME305_Lab0xFF_Part1/Lab0xFF_FrontEndUI.py
@author     Nicholas Greco
@date       4/01/2021
"""
from array import array

## @brief The proportional gain
Kp = None
## @brief Array for system time data
time_Data = array('f', [])
## @brief Array for system position data
pos_Data = array('f', [])
## @brief Array for system velocity data
vel_Data = array('f', [])


# Take the postional and velocity data from the Ctrl_Task and bring that into
# shares, which will be imported by the UI_task, which will pass this data to 
# the front end for plot generation

# Action Items:
    # Google how to access a variable/list in a class
    # Insert the velocity data of the encoder, converted to rad/s, into
    # the line in the back end where the arbitrary function from Part 1 of 
    # the Final Project currently resides

# name of Project: Data Collection Interface