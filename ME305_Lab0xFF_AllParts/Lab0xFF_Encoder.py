# -*- coding: utf-8 -*-
"""@file    Lab0xFF_Encoder.py

@brief      Implements the onboard encoder to count the motion of the motor arm
@details    The encoder is connected to the motor in such a way as to be able
            to monitor the position of the motor arm whenever the encoder
            samples the data. 
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/Lab3/Lab0x03_SimonSays.py
@author     Nicholas Greco
@date       3/05/2021
"""

import pyb
import utime

class Encoder:
    '''@brief Runs operations and returns values specific to the encoder count
       @details Class for updating, getting, setting, and computing the change 
                in the encoder count.
    '''

    # Encoder Driver class that will later be implemented in tasks
    # Static variables are defined outside of methods
    
    pin1 = None 
    pin2 = None
    tim = None
    AngList = []
    DatumCnt = 0
    DeltaCnt = 0
    theta = 0
    def __init__(self, tim, pin1, pin2):
        
        ## @brief The timer from which the encoder counts
        self.tim = tim
        
        ## @brief One of two pins needed for collecting data on motor ops
        self.pin1 = pin1
        ## @brief One of two channels needed for the encoder operation
        self.timCh1 = self.tim.channel(1, pyb.Timer.ENC_A, pin=self.pin1)
        
        ## @brief One of two pins needed for collecting data on motor ops
        self.pin2 = pin2
        ## @brief One of two channels needed for the encoder operation
        self.timCh2 = self.tim.channel(2, pyb.Timer.ENC_B, pin=self.pin2)
        
        ## @brief Initial value that will be used to measure a change in count
        self.DatumCnt = self.tim.counter()
        
    def update(self):
        '''@brief Updates the encoder count and corrects for overflow
           @return theta The position of the encoder arm in ticks
        '''

        ## @brief A count value that will be updated each time update is called
        self.newCnt = self.tim.counter()
        print("New Count: {}".format(self.newCnt))
        
        # The class is constructed 1st, meaning that DatumCnt will be less than
        # newCnt by definition. The difference between newCnt and DatumCnt will 
        # continue to grow the longer the period between which the update 
        # method is called. This will lead to an overflow problem that needs 
        # resolved.
        ## @brief Establishes the change in count
        #  @details The change in count represents the change in the location
        #           of the motor arm between sample periods. This change in 
        #           count conveys direction and velocity data. The count can 
        #           only go so high before an overflow condtion occurs, meaning
        #           we have to correct the overflow in order to get a 
        #           continuous representation of the motor;s motion.
        self.DeltaCnt = self.newCnt - self.DatumCnt
        print("Datum Count: {}".format(self.DatumCnt))
        
        # Corrects the overflow problem
        if self.DeltaCnt < 0xFFFF/2 and self.DeltaCnt > -1*(0xFFFF/2):
            pass
        elif self.DeltaCnt > 0xFFFF/2:
            self.DeltaCnt = self.DeltaCnt - 0xFFFF
        elif self.DeltaCnt < -1*(0xFFFF/2):
            self.DeltaCnt = self.DeltaCnt + 0xFFFF
            
        # This allows us to always be measuring change in count (hence the 
        # delta in DeltaCnt)
        self.DatumCnt = self.newCnt
        # Theta, which will eventually represent the angle of the motor arm, 
        # changes as the count changes, because the changing count represents
        # the measured rotation of the motor arm by the encoder
        self.theta += self.DeltaCnt
        print("theta: {}".format(self.theta))
            
        return self.theta
        # Unsure how to convert from delta, measured by value of the count, to
        # theta, which corresponds to the angle of the motor arm
        
    def getPosition(self):
        '''@brief Gives the last stored position of the encoder arm
           @return theta; The position of the encoder arm in ticks
        '''
        return self.theta
        # alternatively, can use "-1" in place of len(self.AngList) to get the
        # last element of the list
    
    def setPosition(self, reset_value):
        '''@brief Sets the encoder count to a desired value
           @param reset_value; The position one wishes to set the encoder to
        '''
        self.DeltaCnt = reset_value
    
    def getDelta(self):
        '''@brief Returns the change between the last two counts in update
           @return DeltaCnt; The change in the count 
        '''
        print("Change in Count: {}".format(self.DeltaCnt))
        return self.DeltaCnt

if __name__ == "__main__":
    # Program initialization goes here
    tim4 = pyb.Timer(4, period = 0xffff, prescaler = 0) 
    
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    
    enc = Encoder(tim4, pinB6, pinB7)
    time_init = utime.ticks_ms()
    t_sample = 50 # sampling period in milliseconds; not the 492 calculated
    pos_vec = [] # switch to arrays 
    del_pos = []
    time_vec = [time_init]
    # record times to plot against position
    while True:

        if utime.ticks_diff(utime.ticks_ms(), time_init) >= t_sample:
            
            Pos = enc.update()
            print("Position: {}".format(Pos))
            pos_vec.append(Pos)
            
            Del = enc.getDelta()
            print("Delta: {}".format(Del))
            del_pos.append(Del)
            
            time_init = utime.ticks_ms()
            print("Initial_Time: {}".format(time_init))
            time_vec.append(time_init)
            
            Vel = Del/t_sample # values in ticks/ms
        
# A way to solve the problem is to read the timer rapidly and repeatedly, and 
# each time compute the change, or delta, in timer count since the previous 
# update(), and add the distance to a Python variable which holds the position. 
# This position should count total movement and not reset for each revolution 
# or when the timer overflows
        
    
    

        
        
                