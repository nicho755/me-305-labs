# -*- coding: utf-8 -*-
"""@file    FrontEnd.py

@brief      Initiates a data collection process and plots the results
@details    Utilizes serial communication to write a specific "go" variable in 
            ASCII form that the back end (UI_task) uses to intiate a data 
            collection process. This file then receives the collected data as a
            CSV, decodes it, and then processes the data to allow for a plot to
            be made of time vs. the arbitrary output
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/ME305_Lab0xFF_AllParts/FrontEnd.py

@image      html Lab0xFF_MotorPlot.jpg
@author     Nicholas Greco
@date       4/04/2021
"""

from matplotlib import pyplot
import serial
## @brief Defines the serial port, the sampling rate, and the timeout
ser = serial.Serial(port='COM6', baudrate=115273, timeout = 15)

print("Enter in a value for your proportional gain.")      
## @brief Allows the user to enter the desired Kp value
Kp_val = input('Kp: ')
print("Kp: " + Kp_val)
ser.write(str("K" + Kp_val + "/r/n").encode()) # look at documentation for input

# init_char = 'g'
# ser.write(str(init_char).encode('ascii'))

# could build in added functionality regarding what the user is allowed to 
# enter; Ex: Labo0x01, CondVar = idx.isdigit() 
 
idx = 0 
smplTime = 50 #ms
smplDur = 5000 #ms, aka 5 s
## @brief A list of the times that will be written over from the BackEnd
TimeList = []
## @brief A list of the output that will be written over from the BackEnd
OutputList = []
# value for idx is dependent on sampling rate and sampling duration.

while idx < smplDur/smplTime:
    ## @brief Reads the data from the UI_task and decodes it from ASCII
    #  @details The string of data the UI_task generates is a CSV containing 
    #           two columns. This CSV is written over to the FrontEnd in ASCII 
    #           form, and needs to be changed into a string by the variable
    #           "myval". The variable is subsequently overwritten as the CSV,
    #           now a string, is stripped of its parenthesis    
    myval = ser.readline().decode()
    print(myval)
    myval = myval.strip()
    myList = myval.split(",")
    print(myList)
    TimeList.append(float(myList[0]))
    OutputList.append(float(myList[1]))
    idx += 1

pyplot.figure()
pyplot.plot(TimeList, OutputList)
pyplot.xlabel('Time [s]')
pyplot.ylabel('Ang Vel [rad/s]')

ser.close()

# Take code from computer, open serial port, configure Python to give serial 
# code, and to read whatever code that comes from the Nucleo, then to format it
# in such a way as to be useful for plotting 