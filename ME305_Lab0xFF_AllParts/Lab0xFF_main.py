# -*- coding: utf-8 -*-
"""@file    Lab0xFF_main.py

@brief      Runs the various tasks to accomplish motor speed control
@details    The main file establishes all the necesary specifications to allow
            for the implementation of the various tasks, including timers,
            channels, timing variales, pins, and other system parameters. The 
            main file then creates objects and implements them.
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/Lab3/Lab0x03_SimonSays.py
@author     Nicholas Greco
@date       4/04/2021
"""

import pyb
import utime
from Ctrl_Task import Ctrl_Task
from UI_BackEnd import UI_BackEnd
import SharesLib

BackEnd = 0
CL_Ops = 1
Shares = 2

# Main should only have the code to build your tasks and run them
if __name__ == "__main__":
    
    pyb.repl_uart(None)
    
    # parameters for the Encoder
    ## @brief Sets the timer for the encoder
    tim4 = pyb.Timer(4, period = 0xffff, prescaler = 0) 
    ## @brief Sets the pins for the encoder
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    
    ## @brief Sets the rate at which the encoder samples the data
    t_sample = 50
    
    # parameters for the Motor
    ## @brief Sets the sleep pin for the motor
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP) 
    # Assigned sleep pin to pinA15, and set the pin to output mode so that
    # we can read an output from the pin
    
    ## @brief Sets the pins for the motor
    pinB4 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    # Assigning variable pinB4 to B4 on the MCU, and then setting the pin to 
    # be wired for output in push/pull format
    pinB5 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    # Assigning variable pinB5 to B5 on the MCU, and then setting the pin to 
    # be wired for output in push/pull format
    
    ## @brief Sets the timer for the motor
    tim = pyb.Timer(3, freq = 20000)
    # Setting the timer to TIM3 on the MCU, and causing the timer to sample 
    # with a frequency of 20000 Hz
    
    # parameters for the Controller
    ## @brief Sets the Saturation Level for the Controller
    SatLev = 100
    ## @brief Sets the desired angular velocity of the motor
    om_des = 32 # rad/s, or ~300 RPM
    
    ## @brief Creates an object for the BackEnd
    ui_BackEnd = UI_BackEnd()
    state = BackEnd
    while True:
        try: 
            if state == BackEnd:
                if ui_BackEnd.run() is not None: 
                    Kp = SharesLib.Kp
                    ## @brief Creates an object for the Ctrl_Task
                    ctrl_Task = Ctrl_Task(tim4, pinB6, pinB7, t_sample, pin_nSLEEP, tim, pinB4, pinB5, Kp, SatLev, om_des)  
                    state = CL_Ops
            
            if state == CL_Ops:
                if ctrl_Task.run() == Shares:
                    state = Shares
                    
            if state == Shares:
                ## @brief Sends time data from the Ctrl_Task to the SharesLib
                SharesLib.time_Data = ctrl_Task.time_vec
                ## @brief Sends velocity data from Ctrl_Task to SharesLib
                SharesLib.vel_Data = ctrl_Task.ang_vel
                ui_BackEnd.run()
                
        except KeyboardInterrupt:
            # This except block catches Ctrl-C from the keyboard
            # and only broke the while(True) loop when desired
            pin_nSLEEP.low()
            break
        
    # This loop sets the SharesLib vectors equal to the time and angular vel.
    # vectors of the Ctrl_Task. The placement of this loop allows for the 
    # vectors to only need to be generated once (during the operation of the
    # run method of the CLsystem above) before being based to the SharesLib.
    # From here, the UI_Task is run again, and will write over the SharesLib
    # data to the FrontEnd.
    