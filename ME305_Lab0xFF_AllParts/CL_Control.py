# -*- coding: utf-8 -*-
"""@file    CL_Control.py

@brief      Creates a prop. control loop for regulating the motor velocity
@details    The control loop is stuck firmly on the firmware side of things, 
            and is only completed through connections to other methods that
            bridge to the physical world. The control loop attenuates or 
            amplifies based upon the user-defined desired angular velocity, 
            the acutal angular velocity that the encoder returns, and the 
            proportional gain determined by the user.
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/Lab3/Lab0x03_SimonSays.py
@author     Nicholas Greco
@date       3/14/2021
"""

import pyb
import sys

class CL_Control:
    '''@brief Controls motor operations via encoder data and user inputs
       @details Class for updating the PWM level, as well as for getting and
                setting the proportional gain values.
    '''    
    # Encoder Driver class that will later be implemented in tasks
    # Static variables are defined outside of methods

    Kp = 0
    SatLev = 0
    def __init__(self, Kp, SatLev):
        
        ## @brief The proportional gain of the control system
        self.Kp = Kp
        print("Kp: {}".format(self.Kp))

        ## @brief PWM level where desired motor ops exceed the operating cap.
        self.SatLev = SatLev 
        # It would not make sense ot request more than 100% effort from the
        # motor. That said, the motor rotaiton is bidirectional, and therefore
        # the motor must be capable of 100% effort in either direction
        
    def update(self, om_des, om_act):
        '''@brief Updates the PWM level that will be sent to the motor
           @param om_des The desired angular velocity of the motor
           @param om_act The angular velocity conveyed by the encoder
           @return The PWM signal in percentage of motor effort
        '''
        # the PWM signal is determined by the change in velocity multiplied by
        # the proportional gain, which changes the units from RPM to %
        self.L = self.Kp*(om_des - om_act)
        # should be between values of SatLev, on order of 10^2
        if self.L < -1*self.SatLev or self.L > self.SatLev:
            print("PWM LEVEL: {}".format(self.L))
            print('Magnitude of PWM Level Too High. Adjust System Parameters')
            self.L = self.SatLev 
            # make signed, two if's
        return self.L
        
    def get_Kp(self):
        '''@brief Returns the value of the current proportional gain
           @return The prop. gain currently being used in the control loop
        '''        
        return self.Kp
    
    def set_Kp(self, set_value):
        '''@brief Sets the value of the proprtional gain
           @param set_value The new PWM Level
        '''        
        self.Kp = set_value
        
        