# -*- coding: utf-8 -*-
"""@file    UI_BackEnd.py

@brief      Interprets a "go" signal, builds a CSV, & sends it to the FrontEnd
@details    Upon receiving a certain signal from the FrontEnd, the UI_Task FSM
            switches states to begin plugging in a set of times to an arbitrary
            function in order to generate an output vector. Upon reaching 
            3000s, the data is then packaged as a CSV file and written to
            the FrontEnd using serial communication.
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/ME305_Lab0xFF_Part1/Lab0xFF_FrontEndUI.py
@author     Nicholas Greco
@date       4/04/2021
"""

## SOFTWARE HANDSHAKE
import pyb 
from pyb import UART
from array import array
from math import exp, sin, pi
import SharesLib
import sys

class UI_BackEnd:
    '''@brief Performs a data collection, then passes the data to the FrontEnd
       @details Upon receiving a certain signal, the control loop uses the 
                extend method to take the data for time and velocity from 
                SharesLiv, then sends that data via serial communication to the 
                FrontEnd, where it can be graphed.
    '''    
    
    Wait_State = 0
    DataClct_State = 1
    PrtData_State = 2
    
    # pyb.repl_uart(None)
    # Above line intended to prevent print statements from being displayed
    # in the UART
    
    def __init__(self):
        ## @brief Sets up the UART port through which the serial comms occur
        self.myuart = UART(2)
        self.state = self.Wait_State
        # if state == Wait_State:
        #     print('Ive been waiting, waiting, waiting...')
    
        self.cntr = 0
        # The counter for the FSM
        
        ## @brief Creates an array for the times
        self.times = array('f', [])  # sec
        ## @brief Creates an array for the positions
        self.angPos = array('f', []) # rad
        ## @brief Creates an array for the velocities
        self.angVel = array('f', []) # rad/sec
    
    def run(self):
        '''@brief Triggers the data processing upon receiving the "go" prompt
        '''
        if self.state == self.Wait_State:
            # print('Wait_State')
            if self.myuart.any() != 0:
                ## @brief Decodes the FrontEnd comms
                self.init_char = self.myuart.readchar()
                if self.init_char == 75:
                # If receives a 'K', start collecting data
                    ## @brief Decodes the Kp passed from the FrontEnd
                    self.Kp = self.myuart.readline().decode()
# print(self.Kp)
                    self.Kp_adj = self.Kp.strip('/r/n')
# print(self.Kp_adj)
                    self.Kp_float = float(self.Kp_adj)
                    # shares.Kp is a variable that "shadows" the value in the 
                    # BackEnd, in that its value is determined in the BackEnd
                    # first
                    ## @brief Sets the value of Kp in SharesLib
                    SharesLib.Kp = self.Kp_float
# print(self.Kp_float)
                    self.state = self.DataClct_State
                    return SharesLib.Kp
                return None
        
        elif self.state == self.DataClct_State:
            # print('DataClct_State')
            self.times.extend(SharesLib.time_Data)
            # replace the output function below with the velocity function
            # from the encoder (in Ctrl Task, access via SharesLib)
            self.angVel.extend(SharesLib.vel_Data)
            
            # if SharesLib.time_Data[-1] >= 5:
            self.state = self.PrtData_State
            
            print("Time: {}".format(self.times))
            print("Angular Velocity: {}".format(self.angVel))
        
        if self.state == self.PrtData_State:
            # print('PrtData_State')
            if self.cntr <= len(self.angVel) - 1:
                self.myuart.write('{x}, {y} \r\n'.format(x = self.times[self.cntr], y = self.angVel[self.cntr]))
                self.cntr += 1
            else:
                sys.exit(0)
            
            #
        
