# -*- coding: utf-8 -*-
"""@file    Lab0xFF_Encoder.py

@brief      Creates a motor class that operates based on a PWM signal
@details    The motor is constructed using a combination of pins and channels
            to convey information to the physical motor. A sleep pin allows the
            motor to be enabled and disabled. The correct combination of pins,
            channels, and timers allows the board to interface with the MCU,
            receiving data on operation that can be conveyed to the motor
            existing in the physical world.
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/Lab3/Lab0x03_SimonSays.py
@author     Nicholas Greco
@date       3/10/2021
"""

# TIM3, CH 1/2, pin B4/B5
# TIM3, CH 3/4, pin B0/B1
import pyb
import utime

class MotorDriver:
    '''@brief Motor class complete with enable, disable, and set_duty methods
    '''

    pin_nSLEEP = None
    IN1_pin = None
    IN2_pin = None 
    tim = None 
    
    def __init__ (self, pin_nSLEEP, tim, IN1_pin, IN2_pin):
        ## @brief The timer utilized by the motor
        self.tim = tim
        
        ## @brief The pin that can be toggled to turn the motor off
        self.pin_nSLEEP = pin_nSLEEP
        
        ## @brief One of the two pins through which the motor can be controlled
        self.IN1_pin = IN1_pin
        ## @brief One of the two channels conveying PWM signals to the motor
        self.timCh1 = self.tim.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        
        ## @brief One of the two pins through which the motor can be controlled
        self.IN2_pin = IN2_pin
        ## @brief One of the two channels conveying PWM signals to the motor
        self.timCh2 = self.tim.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        print('Creating a motor driver')
    
    def enable (self):
        '''@brief Turns the sleep pin to high, as the motor default is off
        '''
        self.pin_nSLEEP.high()
        # Turning the sleep pin into the "on" position
        print('Enabling Motor')
        
    def disable (self):
        '''@brief Turns the sleep pin to low, disabling the motor
        '''
        self.pin_nSLEEP.low()
        print('Disabling Motor')
        
    def set_duty (self, duty):
        '''@brief Sets the duty to a value passed in by the user
           @details The duty is set by what the user passes in. However, the
                    PWM signal can only be a positive value from 0 to 100. This
                    is accounted for using a conditional statement to pass in a
                    positive value for PWM, while still allowing the motor to
                    turn in both directions.
           @param duty The desired duty cycle at which the motor is to run
        '''
        # This method sets the duty cycle to be sent to the motor to the given 
        # level
        # Positive values cause rotation in one direction, negative values in 
        # the other direciton
        self.duty = duty
        # "duty" will need to be a signed integer holding the duty cycle of 
        # the PWM signal sent to the motor
        if self.duty >= 0:
            self.timCh1.pulse_width_percent(0)
            self.timCh2.pulse_width_percent(self.duty)
            # print(self.duty)
            print('This direction')

        elif self.duty < 0:
            self.timCh1.pulse_width_percent(abs(self.duty))
            self.timCh2.pulse_width_percent(0)
            # print(self.duty)
            print('The opposite direction')
        
if __name__ == '__main__':
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP) 
    # Assigned sleep pin to pinA15, and set the pin to output mode so that
    # we can read an output from the pin
    
    pinB4 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    # Assigning variable pinB4 to B4 on the MCU, and then setting the pin to 
    # be wired for output in push/pull format
    pinB5 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    # Assigning variable pinB5 to B5 on the MCU, and then setting the pin to 
    # be wired for output in push/pull format
    
    tim = pyb.Timer(3, freq = 20000)
    # Setting the timer to TIM3 on the MCU, and causing the timer to sample 
    # with a frequency of 20000 Hz
    motDrvr = MotorDriver(pin_nSLEEP, tim, pinB4, pinB5)
    # Assigning the variable motDrvr to the class MotorDriver and assigning 
    # objects to the various arguments of MotorDriver.

    motDrvr.enable() 
    tStart = utime.ticks_ms()
    while True:
        motDrvr.set_duty(30)
        if utime.ticks_diff(utime.ticks_ms(), tStart) >= 5000:
            motDrvr.disable()
        # Setting the duty cycle of the motor driver class to a certain value

        

    