# -*- coding: utf-8 -*-
"""@file    Ctrl_Task.py

@brief      Implements the component drivers of the system into a single task
@details    The control task defines a series of methods that implement the
            encoder, motor, and control loop into a cohesive unit that can 
            function as a control system to a given input. The CtrlTask 
            contains an FSM, which prevents the run method from returning
            values for the time, position, and velocity before a value for the
            proportional gain is specified. The code terminates when the arrays
            reach a certain length, determined by the sample time and the 
            arbitrary run time.
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/Lab3/Lab0x03_SimonSays.py
@author     Nicholas Greco
@date       2/04/2021
"""
from Lab0xFF_Encoder import Encoder
from MotorDriver import MotorDriver
from CL_Control import CL_Control
from array import array
import math
import utime
import SharesLib

class Ctrl_Task:
    '''@brief Imports control loop objects to fill position & velocity arrays
       @details Creates an encoder, motor, and control loop objects, then 
                preallocates arrays for storing the data they return. Fills 
                the arrays with this data, and implements a counter to ensure
                the arrays are the intended length.
    '''        
    WaitForKp = 0
    MotorKick = 1
    mainFile_Shares = 2
    
    runTime = 5000
    # csvTime = array('f', [])
    # csvVel = array('f', [])
    # csvPos = array('f', [])
    # all vectors are saved for purpose of later plotting 
    
    def __init__(self, tim4, pin6, pin7, t_sample, pin_nSLEEP, tim, pin4, pin5, Kp, SatLev, om_des):
        
        ## @brief Creates an encoder object
        self.EncoderObj = Encoder(tim4, pin6, pin7)
        ## @brief Creates a closed loop object
        self.CLoopObj = CL_Control(Kp, SatLev)
        ## @brief Creates a motor object 
        self.MotorObj = MotorDriver(pin_nSLEEP, tim, pin4, pin5)
        
        ## @brief Sets the time at which the drivers sample the data
        self.t_sample = t_sample
        
        ## @brief Preallocated array for position
        self.pos_vec = array('f', [0 for i in range(int(self.runTime/self.t_sample))])
        ## @brief Preallocated array for change in position
        self.del_pos = array('f', [0 for i in range(int(self.runTime/self.t_sample))])
        ## @brief Preallocated array for time
        self.time_vec = array('f', [0 for i in range(int(self.runTime/self.t_sample))])
        ## @brief Preallocated array for velocity
        self.ang_vel = array('f', [0 for i in range(int(self.runTime/self.t_sample))])
        
        ## @brief The desired angular velocity of the motor
        self.om_des = om_des
        
        ## @brief Enables motor operations
        self.MotorObj.enable()
        
        self.newTime = utime.ticks_ms()
        self.absTime = utime.ticks_ms()
        self.time_vec.append(self.newTime) 
        self.state = self.WaitForKp
        
        self.cntr = 0
        
        # self.csvTime
        # self.csvVel
        # self.csvPos
   
    def run(self):
        '''@brief Fills arrays w/ data from component drivers of the ctrl syst.
           @details Upon receiving a non-None value for Kp, values from the
                    drivers begin to populate the arrays, the PWM signal for 
                    the motor is set, and the timers are updated. This occurs
                    until a pre-determined array length is reached (a function
                    of the sample time and run time), at which point the method
                    returns a state that is usd in main to shift out of
                    the state operating the run method herein.
        '''    
        # FSM employed so that the Ctrl_Task only operates once Kp is passed
        # in by the user
        if self.state == self.WaitForKp:
            if SharesLib.Kp is not None:
                ## @brief The value of the prop. gain as contained in SharesLib
                self.Kp = SharesLib.Kp
                self.state = self.MotorKick
        
        if self.state == self.MotorKick:
            if utime.ticks_diff(utime.ticks_ms(), self.newTime) >= self.t_sample:
                
                ## @brief The means for converting from radians to ticks
                self.conv2rad = (2*math.pi)/4000 # rad/ticks
                
                # self.pos_vec.append(self.EncoderObj.update()) #*self.conv2rad # we want units in deg
                # self.del_pos.append(self.EncoderObj.getDelta())
                
                ## @brief Fills specific index w/ the encoder position data
                self.pos_vec[self.cntr] = self.EncoderObj.update()
                ## @brief Fills specific index w/ del. position encoder data
                self.del_pos[self.cntr] = self.EncoderObj.getDelta()
                
                print("Delta_Cnt: {}".format(self.del_pos[self.cntr]))
                
                ## @brief Computes the ang. vel. from encoder data in rad/s
                self.om_act = (self.del_pos[self.cntr]*self.conv2rad)/(self.t_sample*0.001) 
                print("om_act: {}".format((self.om_act)))
                ## @brief Populates an array for the actual angular velocity
                self.ang_vel[self.cntr] = self.om_act
                
                ## @brief The PWM signal to be sent to the motor
                PWM = self.CLoopObj.update(self.om_des, self.om_act)
                print("PWM: {}".format(PWM))
                # The following line adds the desired PWM value (the om_des divided
                # by Kp) and adds it to the PWM signal that results from a nonzero
                # difference existing between the motor velocity and the encoder 
                # velocity
                self.MotorObj.set_duty(PWM) 
                # self.om_des/self.Kp; not needed because level of imperfection
                # of control loop makes it so that the controller never 
                # approaches zero
                self.newTime = utime.ticks_ms()
                self.time_vec[self.cntr] = self.newTime
                self.cntr += 1
                
                # if utime.ticks_diff(utime.ticks_ms(), self.absTime) >= 5000:
                if self.cntr >= len(self.pos_vec):
                    # print("Time Vector: {}".format(Ctrl_Task.time_vec))
                    print("Velocity Vector: {}".format(self.ang_vel))
                    # print("Velocity Vector Length: {}".format(len(self.pos_vec)))
                    self.MotorObj.disable()
                    return self.mainFile_Shares 


    # def EncoderTask(self, time):
    #     self.time_vec.append(time) 
    #     self.pos_vec.append(self.EncoderObj.update())
    #     self.del_pos.append(self.EncoderObj.getDelta())
        
    #     self.conv2rad = (2*math.pi)/4000 # rad/ticks
        
    #     print("Delta_Cnt: {}".format(self.del_pos[-1]))
    #     print("Velocity: {}".format((self.del_pos[-1]*self.conv2rad)/self.t_sample))
        
    #     return (self.del_pos[-1]*self.conv2rad)/(self.t_sample*0.001)
    
    # def MotorTask(self, duty):
    #     self.MotorObj.set_duty(duty)
        
    # def CLoopTask(self, om_des, om_act):
    #     return self.CLoopObj.update(om_des, om_act)
