# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 13:15:33 2021

@author: nicho
"""
from matplotlib import pyplot
import serial
ser = serial.Serial(port='COM6', baudrate=115273, timeout = 3)
# increasing timeout from 1 to 3 seconds
# increase timeout length or put delay after G but before data is read
# timeout value is the duration the serial port waits for an input before 
# simply returning an empty string

# Objective:send a G and read from serial port until run out of data

# def sendChar():
#     inv = input('Give me a character:')
#     ser.write(str(inv).encode('ascii'))
 
# for n in range(1):
#     print(sendChar())        

inv = 'g'
ser.write(str(inv).encode('ascii'))
 
idx = 0 
TimeList = []
OutputList = []
while idx < 3000:
    myval = ser.readline().decode('ascii')
    myval = myval.strip("()")
    myList = myval.split(",")
    TimeList.append(float(myList[0]))
    OutputList.append(float(myList[1]))
    idx += 1

pyplot.figure()
pyplot.plot(TimeList, OutputList)
pyplot.xlabel('Time [s]')
pyplot.ylabel('Output')

ser.close()

# Take code from computer, open serial port, configure Python to give serial 
# code, and to read whatever code that comes from the Nucleo, then to format it
# in such a way as to be useful for plotting 
    



    
