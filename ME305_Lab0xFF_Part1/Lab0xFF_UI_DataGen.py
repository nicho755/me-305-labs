# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 13:36:27 2021

@author: nicho
"""

# ampy --port COM6 put Lab0xFF_UI_DataGen.py main.py

## SOFTWARE HANDSHAKE
import pyb 
from pyb import UART
from array import array
from math import exp, sin, pi

class BackEnd:
    
    Wait_State = 0
    DataClct_State = 1
    PrtData_State = 2
    
    # pyb.repl_uart(None)
    # Above line intended to prevent print statements from being displayed
    # in the UART
    
    def __init__(self):
        
        self.myuart = UART(2)
        self.state = self.Wait_State
        # if state == Wait_State:
        #     print('Ive been waiting, waiting, waiting...')
        
        
        self.time = 0 
        # Takes us through 30s of motor operation
        self.time_del = 0.01
        # The change in time between each index in the time array
        self.cntr = 0
        # The counter for the FSM
        # rename val
        self.times = array('f', [])
        self.outputs = array('f', []) 
    
    def operate(self):

        if self.state == self.Wait_State:
            if self.myuart.any() != 0:
                self.val = self.myuart.readchar()
                if self.val == 103:
                # If receives a 'g', start collecting data
                    self.state = self.DataClct_State
        
        elif self.state == self.DataClct_State:
            self.times.append(self.time)
            # times.insert(time, new_time)
            # values.insert(time, exp(-0.1*new_time)*sin((2*pi)/3)*new_time)
            self.outputs.append(exp(-0.1*self.time)*sin((2/3)*pi*self.time))
            # For each value of the counter through the length of the value array,
            # a value based on the function above will be inserted into the 
            # corresponding index of the value array
            self.time += self.time_del
            if self.time >= 30:
            # The counter starts at 0 and increments up to 3000 values. The 2999-th
            # value of cntr consitutes the 3000-th instance of cntr, thus the 
            # value array is saturated and the data collection phase is at an end.
            # In order to move the FSM to the next state, I associate the length of
            # the value array with a final value of the counter
                self.state = self.PrtData_State
        
        if self.state == self.PrtData_State:
    
            self.myuart.write('{x}, {y} \r\n'.format(x = self.times[self.cntr], y = self.outputs[self.cntr]))
            self.cntr += 1
        
        # if self.cntr == len(self.times):
        #     return None 
                
if __name__ == "__main__":              

    myTask = BackEnd()
    while True:
        myTask.operate()
        
            
        
        
                
                
                
                
                
          