# -*- coding: utf-8 -*-
"""@file    ME405_HW0x01.py

@brief      Returns change in smallest quantity of bills using input's collection type 
@details    The function takes in arguments of price and denomination, the 
            former being an "int" and the latter being a collection type that
            includes every denomination that the user can pay with, and
            subsequently in which they can be given change. The programming 
            method employed below starts at the largest bill denomination and
            proceeds to evaluate the number of bills that can be returned for
            the change that is due, given that fractional bill values are
            nonsensical. The user is returned their change in exactly the
            collection type within which they specified their payment.
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/ME405_HW0x01_changeMaking/ME405_HW0x01.py
@author     Nicholas Greco
@date       4/06/2021
"""

import math

def getChange(price, denominations):
    '''@brief Gets proper change based on user payment amount & collection type
       @param price The numeric value of the item being purchased
       @param denominations The collection type representing the bills used to pay for the item
       @return The change due in the same collection type as the input
    '''
    ## @brief The sum of the bill types passed in for the purchase
    payment = 0
    for key, value in denomDict.items():
        payment += float(key)*value  

    ## @brief The list of possible denominations
    denomList = [20, 10, 5, 1, 0.25, 0.1, 0.05, 0.01]
    ## @brief The change due based upon the difference b/w the price & payment
    changeDue = payment - price
    for denom in denomList:
        ## @brief Computes integer number of bills 
        numBill = int(changeDue/denom)
        print(numBill)
        ## @brief Returns the modulus of the change and the current denom.
        billMod = math.fmod(changeDue, denom)
        changeDue = billMod
        denomDict[str(denom)] = numBill
        
    return denomDict

if __name__ == "__main__":
    
    ## @brief Collection type specifying bill types and numbr. used for payment
    denomDict = {
            "20" : 2,
            "10" : 0,
            "5" : 0,
            "1" : 0,
            "0.25" : 0,
            "0.1" : 0,
            "0.05" : 0,
            "0.01" : 0
                        }
    ## @brief The price of the arbitrary item
    price = 34
    
    getChange(price, denomDict)
    print(denomDict)
            
        
        
    
        
        
        
    