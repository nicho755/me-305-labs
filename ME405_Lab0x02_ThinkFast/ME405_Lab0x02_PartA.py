'''@file        LAB0X02A_ME405.py
@brief          This file computes the average reaction time for the user to
                press a button after an LED turns on.
                
@details        This file is what is used to calculate the individual reaction 
                time per round and the average reaction time at the end of the
                game for the user. When the user initializes the game, they will
                recieve directions on how to play and how to interact with the 
                game. When enter is pressed the round begins and the user is 
                alerted to be on the lookout for the LED flash. As soon as the
                LED turns on, the user must press the blue button B2 to turn the
                LED off, and the reaction time is recorded. If the user does not
                push the button after a second that the LED is on, it will turn
                off and continue to measure the reaction time. If the time from
                when the LED turned on to when the user pushes the button is 
                greater than five seconds, the user will be notified that the 
                round timed out, and the round is not counted. Once the program 
                is started it will continue to run until it is ethier stopped 
                by the debug window or by exiting pressing Ctrl+C. When the user 
                presses Ctrl+C, the game is ended and it returns an average 
                reaction time to the user as well as the number of rounds that 
                were played.
                
                The documented source code can be found at the following link:
                
@author         Davide Lanfranconi, Nicholas Greco
@date           04/30/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved'''

# Import modules needed to run code on Nucleo
import pyb
import utime
import random

# Define timer and Pin locations and rename for quick reference
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
state = 0
t = True
ButtonAction = 0
reactiontimes = [0]

#function for the user button. It is the callback function when the button is pressed    
def ButtonPressIsPattern(IRQ_src):
    '''@brief This function is the callback function for when the button is pressed
       @details This takes in a parameter IRQ_src which recognizes the falling edge. 
       Button Action is a variable that indicates when the button is pressed
       @param IRQ_src
    '''
    global ButtonAction
    ButtonAction = 1

#External interrupt for pyboard for blue user button
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=ButtonPressIsPattern)

def RandTime():
    return random.uniform(2,3)

while True:
    try:
        if state == 0:
            print('Welcome to the User Reaction Game. ')
            print('The objective of the game is to push the blue button as quickly'
            'as possible after the LED turns on.')
            last_key = input('Press enter to begin and Ctrl-C to end the game')
            state = 1
    
        elif state == 1:
            if t:
                print('the round will begin in a few seconds')
                r = RandTime()
                t = False
                starttime = utime.ticks_ms()
                cyclestart = True
                cycleend = True
                ledon = utime.ticks_us()
                ledoff = utime.ticks_us()
                
            elif utime.ticks_diff(utime.ticks_ms(), starttime) >= r*1000 and cyclestart:
                cyclestart = False
                ledon = utime.ticks_us()
                t2ch1.pulse_width_percent(100) 
                cycleend = False
                           
            elif utime.ticks_diff(utime.ticks_us(), ledon) >= 5000000:
                print('The round has timed out, please wait and try again')
                state = 3
                
            elif utime.ticks_diff(utime.ticks_us(), ledon) >= 1000000 and not cycleend:
                t2ch1.pulse_width_percent(0)
                cycleend = True
            
            elif ButtonAction == 1 and not cyclestart:
                ledoff = utime.ticks_us()
                t2ch1.pulse_width_percent(0)
                ButtonAction = 0
                t = True
                state = 2
                
        elif state == 2:
            reaction = ledoff - ledon
            reactiontimes.append(reaction)
            state = 1
            
        elif state == 3:
            t = True
            state = 1
        
    except KeyboardInterrupt:  # will stop the program with a control-C interrupt and display the print line below
        n = len(reactiontimes) - 1
        if n == 0:
            print('Thank you for playing. You have no average time as no rounds were played.')
            break
        elif n > 0:
            reactave = sum(reactiontimes) / (n*1000000)
            print('Thank you for playing. Your average reaction time was', reactave, 'seconds over the course of', n, ' rounds.')
            t2ch1.pulse_width_percent(0)  # Will set LED brightness to 0 before ending program
            break 