'''@file        ME405_Lab0x02_PartB.py
@brief          Uses an OC/IC method to compute the average reaction time for the user to
                press a button after an LED turns on.
                
                
@author         Nicholas Greco, Davide lanfranconi
@date           05/02/21
'''

import pyb
from pyb import Timer

## @brief A counter that increments each time the user presses the button while the LED is on
successCntr = 0
## @brief A counter that is added to whenever the user mistimes their button press
failCntr = 0

correctPress = None
isBP = False
## @brief Specifies the initial count value at which a compare match will trigger
lastCompare = 20000
## @brief The variable to which the count value associated with the button press will be appended
lastCapture = None

ledOn = 0
ledOff = 0


def OC_Compare(tim2):
    '''@brief Callback func. that sets output compare value and evaluates button push
       @details A collection of global variables are used to evaluate whether
                or not the button was pushed at the appropriate time. The LED
                is cycled in 1 seocnd intervals. While the LED is on, if the
                user hits the button, their victory will be recorded.
    '''
    # designate lastCompare a global variable so that we can use it anywhere
    global lastCompare
    global isBP
    global correctPress
    
    print('Entering OC_Compare')
    # Set pinA5 to high
    
    # the compare method gets us a count value from channel 1 of timer 2, which
    # we assign to the lastCompare variable. 
    #lastCompare = t2ch1.compare()
    
    t2ch1.compare(lastCompare)
    # in an if-elif statement, the elif will only run if the initial "if" statement
    # condition wans't met, but the elif's condition was.
    state = ledOn
    if state == ledOn:
        if isBP == True:
            print(isBP)
            correctPress = False
            isBP = False
            print("Button pressed too soon")
        # Adding the appropriate number of counts so that the next compare match 
        # will occur after 1 second
        lastCompare += 32653
        state = ledOff
        
    
    elif state == ledOff:
        if isBP == True:
            correctPress = True
            isBP = False
            print("Success the button was pressed on time!")
            
        # Adding the appropriate number of counts so that the next compare match 
        # will occur after 1 second      
        lastCompare += 32653
        state = ledOn
    
    # This "if" statement corrects for the overflow that eventually onsets once
    # the timer counts beyond 0xffff
    if lastCompare > 0xffff:
       lastCompare -= 0xffff  
    
def IC_Capture(tim2):
    '''@brief Callback func. that triggers when the button is pressed
       @details The input capture function sets the value of a lastCapture variable
                to the time when the button was pressed. Additionally, it sets
                a global variable serving as a conditional to a value of "True",
                which will be used in the OC_Compare callback to evaluate if the
                button was pressed in the appropriate amount of time.
    '''
    # INput capture serves to capture the falling edge only 

    global lastCapture
    global isBP
    print("Capturing the Input")
    # 
    lastCapture = t2ch2.capture()
    #rxnTime = lastCapture*deltaT - lastCapture
    isBP = True
    print(isBP)
    
if __name__ == '__main__':   
    # pinA5 corresponds ot the LED that will toggle when the output compare
    # condition is matched
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    pinB3 = pyb.Pin(pyb.Pin.cpu.B3)
    # value of 2450 is determined to give a time to overflow of approximately 2 seconds
    ## @brief Establishes the timer used
    tim2 = pyb.Timer(2, period = 0xffff, prescaler = 4900) # 2441
    
    # t2ch1 = tim2.channel(1, pyb.Timer.OC_FORCED_INACTIVE, pin=pinA5, callback = None)

    ## @brief Sets timer to IC mode, then sets relavant pins, triggering periods, and callbacks
    t2ch2 = tim2.channel(2, pyb.Timer.IC, pin=pinB3, polarity = Timer.FALLING, callback = IC_Capture)

    try:
        ## @brief Sets timer to OC mode, then sets relavant pins, triggering periods, and callbacks
        t2ch1 = tim2.channel(1, pyb.Timer.OC_TOGGLE, pin=pinA5, callback = OC_Compare)
        # Setting the compare value
        t2ch1.compare(lastCompare)
        while True:
            # Upon a compare match begin obtained between lastCompare and the counter of
            # channel 1 of timer 2, the callback function should initiate
            t2ch1.compare()
            # Using the capture method to trigger a callback of the IC_Capture function
            # whenever the button associated with PB3 is pressed
            t2ch2.capture()
            
            if correctPress == True:
                successCntr += 1
                correctPress = None
            elif correctPress == False:
                failCntr += 1
                correctPress = None
                  
    except KeyboardInterrupt:  # will stop the program with a control-C interrupt and display the print line below
        tim2.deinit()
        # pyb.Pin(pyb.Pin.cpu.A5) # see pyb pin library to reset pin appropriately
        print(successCntr)
        print(failCntr)
        totalGames = successCntr + failCntr
        ## @brief Tabulates the number of games the user played successfully
        rateOfSuccess = successCntr/totalGames
        

            


    
    
    
    
    
    




