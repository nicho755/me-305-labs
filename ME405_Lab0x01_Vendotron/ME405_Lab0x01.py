"""@file    ME405_Lab0x01.py

@brief      A vending machine simulating drink selection & payment via keyboard entries 
            
            <B>source code:</B> https://bitbucket.org/nicho755/me-305-labs/src/master/ME405_Lab0x01_Vendotron/ME405_Lab0x01.py

@image      html ME405_Lab0x01_FinalFSM.jpg
@author     Nicholas Greco
@date       4/18/2021
"""
from time import sleep
import keyboard 
import math
from time import time

# global variable
last_key = ''

# Create states with specific names so that state transitions are not using
# "magic" numbers that do not clearly indicate their purpose
WelcomeState = 0
DrinkNCoin = 1
ComputeVend = 2
Eject = 3

def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

def VendTask(denomDict):
    """ @brief Allows for the purchase of drinks with certain denominations
        @details Keyboard callbacks enable the user to select a desired drink 
                 and to pay for said drink from a fixed variety of denominations.
        @param denomDict    Lists the possible denominations, from largest to 
                           smallest, that the user can pay with. Their values
                           are integers specified in dollar amounts.
        @return Returns the current state of the VendTask FSM               
    """
    global last_key
    # Tell the keyboard module to respond to these particular keys only
    keyboard.on_release_key("C", callback=kb_cb)
    keyboard.on_release_key("P", callback=kb_cb)
    keyboard.on_release_key("S", callback=kb_cb)
    keyboard.on_release_key("D", callback=kb_cb)
    keyboard.on_release_key("E", callback=kb_cb)
    keyboard.on_release_key("Q", callback=kb_cb)
    
    keyboard.on_release_key("0", callback=kb_cb)
    keyboard.on_release_key("1", callback=kb_cb)
    keyboard.on_release_key("2", callback=kb_cb)
    keyboard.on_release_key("3", callback=kb_cb)
    keyboard.on_release_key("4", callback=kb_cb)
    keyboard.on_release_key("5", callback=kb_cb)
    keyboard.on_release_key("6", callback=kb_cb)
    
    state = WelcomeState
    
    ## @brief Establishes the name of each drink and its associated pr
    drinkDict = {
            "Cuke" : 1.00,
            "Popsi" : 1.20,
            "Spryte" : 0.85,
            "Dr.Pupper" : 1.10
                        }
    ## @brief List of denominations the user can pay with
    denomValues = [0.01, 0.05, 0.10, 0.25, 1, 5, 10]

    while True:
        # The finite state machine cycles between a welcome state which resets
        # the currentBal, a state that couples payment and drink selection, a
        # state for computing that the appropriate amount has been paid (and 
        # for subsequently vending drinks), and a state for allowing the user
        # to cancel their transaction and reclaim their change.
        
        if state == WelcomeState:
            # This state introduces the user to the vending machine. It prints
            # the welcome message, sets (or resets) the current balance to zero,
            # and switches the user automatically to the DrinkNCoin state
            print("Welcome to the Vendotron! Please select your favorite refreshing beverage")
            ## @brief The initial time at which the Vendotron starts up; used to determine inactivty
            # time1 = time.time()
            ## @ brief The balance the user has entered into the machine
            currentBal = 0
            state = DrinkNCoin
        
        if state == DrinkNCoin:
            # Allows the user to select their drink of choice and to pay for it
            # using the theoretical coinage they have available, as long as the
            # denominations are American and do not exceed $10 in value
                
            if last_key == 'C':
                ## @brief Extracts the drink price from the drink dictionary
                drinkPrice = drinkDict["Cuke"]
                # drinkType = select Key from drinkDict
                last_key = ''
                state = ComputeVend
                print("For a Cuke, please insert $1.00")
                
            elif last_key == 'P':
                drinkPrice = drinkDict["Popsi"]
                last_key = ''
                state = ComputeVend
                print("For a Popsy, please insert $1.20")
                
            elif last_key == 'S':
                drinkPrice = drinkDict["Spryte"]
                last_key = ''
                state = ComputeVend
                print("For a Spryte, please insert $0.85")  
                
            elif last_key == 'D':
                drinkPrice = drinkDict["Dr.Pupper"]
                last_key = ''
                state = ComputeVend
                print("For a Dr.Pupper, please insert $1.10")
                
            elif last_key == 'E':
                last_key = ''
                state = Eject


            if last_key == '0':
                last_key = denomDict['Penny']
                print(last_key)
                currentBal += last_key
                last_key = ''
                print("You inserted a penny")
                
            elif last_key == '1':
                last_key = denomDict['Nickel']
                currentBal += last_key
                last_key = ''
                print("You inserted a nickel")
                
                
            elif last_key == '2':
                last_key = denomDict['Dime']
                currentBal += last_key
                last_key = ''
                print("You inserted a dime")
                
                
            elif last_key == '3':
                last_key = denomDict['Quarter']
                currentBal += last_key
                last_key = ''
                print("You inserted a quarter")
                
                
            elif last_key == '4':
                last_key = denomDict['Washington']
                currentBal += last_key
                last_key = ''
                print("You inserted a Washington")
                
                
            elif last_key == '5':
                last_key = denomDict['Lincoln']
                currentBal += last_key
                last_key = ''
                print("You inserted a Lincoln")
                
                
            elif last_key == '6':
                last_key = denomDict['Hamilton']
                currentBal += last_key
                last_key = ''
                print("You inserted a Hamilton")
                
                
            # elif last_key == '':
            #     if time.time_diff(time.time(), time1) > 5:
            #         print("Message to be completed")
            #     # on the next iteration, the FSM will run state 1


        elif state == ComputeVend:
            # Determines whether or not the user has entered sufficient funds 
            # for their beverage. If so, their drink is vended, and they will be 
            # presented (or prompted) with a chance to purchase another drink. 
            # If not, they wwill be prompted to enter additional change and 
            # sent back to the DrinkNCoin state.
            if currentBal < drinkPrice:
                state = DrinkNCoin
                # Display the current balance relative to the drink price
                print("Insufficient funds: {}/{}".format(currentBal, drinkPrice))
            else:
                print('Vending Drink Now!')
                # subtract from theccurrent balance the price of the drink
                currentBal -= drinkPrice

                # tell the user what balance remains
                print("Your remaining balance is: {}".format(currentBal))
                if currentBal > 0:
                    # prompt the user to purchase additional beverages if they 
                    # have a remaining (i.e. nonzero balance)
                    print("Apply your remaining balance towards another refreshing beverage! Select another favorite now!")
                    state = DrinkNCoin
                    
                else:
                    state = WelcomeState
        
        elif state == Eject:
            # This state returns the remaining balance of the user in the least
            # number of denominations possible.
            print("Returning your remaining change now!")
            
            ## @brief An empty dictionary to be filled with the returned change
            returnDict = {
                    "10" : 0,
                    "5" : 0,
                    "1" : 0,
                    "0.25" : 0,
                    "0.1" : 0,
                    "0.05" : 0,
                    "0.01" : 0
                                }
            
            for idx in range(len(denomValues)):
                # iterates through the list denomValues, using modulus math
                # to return the smallest number of coins possible. Because the
                # dictionary is organized top down, and the list is organized 
                # left to right, the indexing through the denomValues list has
                # to occur in reverse.
                
                ## @brief Computes integer number of bills 
                #  @details By taking the integer of the current balance divided
                #           by an index in the denomValues list, this ensures
                #           that any denominations that are not greater than the
                #           remaining balance will not be included in the final
                numBill = int(currentBal/denomValues[-1*idx + -1])
                print(numBill)
                ## @brief Returns the modulus of the change and the current denom.
                #  @details When a 
                billMod = math.fmod(currentBal, denomValues[-1*idx + -1])
                print(billMod)
                currentBal = billMod
                # Sets the value in the dictionary to the number of bills 
                # entered. These bills are associated with the dictionary key 
                # representing the corresponding denomination.
                returnDict[str(denomValues[-1*idx + -1])] = numBill

            # Represents the user receiving their change in the coin return.
            # Each denomination present would be returned with the shown quantity.
            print(returnDict)
            print("Please take your change!")
            state = WelcomeState
            
        else:
            # this state shouldn't exist!
            pass   
        
        yield(state)
        
if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object
    denomDict = {
            "Hamilton" : 10,
            "Lincoln" : 5,
            "Washington" : 1,
            "Quarter" : 0.25,
            "Dime" : 0.10,
            "Nickel" : 0.05,
            "Penny" : 0.01
                        }
    ## @brief Creates a generator object
    #  @details Creating a generator object will allow us to iterate through
    #           this theoretically infinite sequence per cycle of the FSM.
    getDrink = VendTask(denomDict)
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    try:
        while True:
            # print('Checking if yield')
            # Sends us back into the FSM, which will pick up right back at the
            # corresponding yield function in the FSM
            next(getDrink)
            sleep(0.01)
            
    except KeyboardInterrupt:
        keyboard.unhook_all () # un-does the callbacks
        print('Ctrl-c detected. Goodbye')