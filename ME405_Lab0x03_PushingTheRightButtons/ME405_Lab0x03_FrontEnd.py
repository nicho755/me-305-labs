"""@file    ME405_Lab0x03_FrontEnd.py

@brief      Passes a signal to initiate data collection, and then plots the recieved data
@details    The Front End allows the user to pass a "go" command to the Back End.
            The Back End receives this code and begins a data collection process.
            This data is then sent back to the Front End, which reads the data, 
            clears away irrelevant characters, and prepares the data to be exported 
            as a CSV. The data is also plotted.
            
            <B>source code:</B> 
@date       5/06/2021
"""

from matplotlib import pyplot
# import pandas as pd
import serial

## @brief Defines the serial port, the sampling rate, and the timeout
ser = serial.Serial(port='COM4', baudrate=115273, timeout = 1) 

## @brief Enables the user to enter a command at the beginning of the program 
#  @details This command will initiate data colleciton in the Back End
goCmd = input('Enter a "g" to begin data collection')
ser.write(str(goCmd).encode('ascii'))

## @brief Creates an empty list to be filled with TIME STAMPS from the BackEnd
TimeList = []
## @brief Creates an empty list to be filled with VOLTAGES from the BackEnd
DataList = []

while ser.in_waiting == 0:
    ## @brief Reads the data from the UI_task and decodes it from ASCII
    #  @details The string of data the UI_task generates is a CSV containing 
    #           two columns. This CSV is written over to the FrontEnd in ASCII 
    #           form, and needs to be changed into a string by the variable
    #           "myval". The variable is subsequently overwritten as the CSV
    #           now as a string is stripped of its parenthesis
    pass 


with ser:
    for line in ser:
        ## @brief Creating a 2 column list to be filled with Back End values
        #  @details This list will be decoded from a byte array to a string.
        #           It will then be stripped of carriages returns and split
        #           at the commas to prepare it to be transferred into a CSV
        [Time, Data] = ser.readline().decode('ascii').strip("()").split(",")
        
        TimeList.append(float(Time))
        # This will change the Data List from bit values to voltages
        DataList.append(float(Data)*(3.3/4095))
        print("t=", Time)
        print("d=", Data)

   
pyplot.figure()
pyplot.plot(TimeList, DataList)
pyplot.xlabel('Time [s]')
pyplot.ylabel('Output')

ser.close()



