"""@file    ME405_Lab0x03_BackEnd.py

@brief      Interprets a "go" signal, and collects ADC data from a charging capacitor
@details    The Back End enables a pin (A0) to interpret the charging of a capacitor
            using ADC functionality. This pin is jumpered to pinC13 so that a button
            press can initiate the data collection. The data is collected in bits,
            which can be converted into voltages.
            
            <B>source code:</B> 
@date       5/06/2021
"""

# ampy --port COM6 put Lab0xFF_UI_DataGen.py main.py

## SOFTWARE HANDSHAKE
import pyb 
from pyb import UART
from pyb import ADC
from array import array

# Intended to prevent print statements from being displayed in the UART
pyb.repl_uart(None)

if __name__ == "__main__":
    ## @brief Setting pin A0 (non-Arduino pin) as the pin to collect ADC data 
    pinADC = pyb.Pin (pyb.Pin.cpu.A0)
    ## @brief Creates a timer object at Timer 6 that triggers at 40 kHz
    #  @details The timer needs to run quick enough to cpature all 4095 points 
    #           we desire in the span of 1 button push 
    tim6 = pyb.Timer(6, freq = 50000)
    
    ## @brief Sets the pin to ADC mode and stores the values in the variable "adc"
    adc = ADC(pinADC)
    
    ## @brief Sets up the UART port through which the serial comms occur
    myuart = UART(2)
    ## @brief The time variable, which will be incremented using the period
    time = 0
    ## @brief The period of the data collection operation
    #  @details This value is determined by dividing the total window that data
    #           collection will occur in (4*tau = 2ms) and dividing this by 
    #           the number of data points (100) to be collected in that time span.
    period = 20 # period in microseconds
    
    ## @brief The array in which the ADC values will be stored
    #  @details This array is termed a buffer because it will store 50 times as
    #           many values as we theoretically need to collect. This buffer is
    #           necessary because it is not likely for our data collection period
    #           to perfectly align with the values we desire to capture.
    buffy = array('H', (0 for index in range (5000)))

            
    while True:
        try:
            if myuart.any() != 0:
                ## @brief Sets the data collection start variable 
                #  @details The start variable is set to whatever the user 
                #           passes in from the front end code.
                goButton = myuart.readchar()
                print(goButton)
                # If receives a 'g', start collecting data
                if goButton == 103:
                    print("Button pressed") 
                    # value of 3890 reduced from 4095 bits in idealized scenario because
                    # the full 4095 won't necessarily be captured
                    while buffy[-1] - buffy[0] < 3890:
                        # Important to match to time constant so that appropriate number
                        # of values (100 points) are captured as the capacitor charges.
                        # Divide 100 points by the settling time (0.5ms*4 = 2 ms) to get
                        # the frequency (50 kHz)
                        adc.read_timed(buffy, tim6)
                        print('Preparing to Write Data')
                
            
                    for data in buffy:
                        myuart.write('{t}, {d} \r\n'.format(t = time, d = data))
                        print('{t}, {d}'.format(t = time, d = data))
                        print(buffy)
                        time += period
                        print(time)


        except KeyboardInterrupt:
            print(" Ctrl+c detectd: Now leave")