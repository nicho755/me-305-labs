# -*- coding: utf-8 -*-
"""@file    Lab0x01_Fibonnaci.py
@brief      Program for Fibonnaci Code
@details    Implements an explicit script in the manner of Fibonnaci
            into a program that allows the user to obtain the Fibonnaci number
            of an integer index
@author     Nicholas Greco
@date       1/21/2021

"""
def fib(idx):

    fibsum = [0, 1]
    for i in range(2,idx+1):
        fibsum.append(fibsum[i-1] + fibsum[i-2])
    
    return(fibsum[idx])

# An alternative means of solving the assignment using a recursive method.
# NOT a good means to limit computation time
  
# def fib2(ind):
    
   # if ind == 0:
        # return 0
    
   # if ind == 1:
       # return 1
    
   # return fib2(ind-1) + fib2(ind-2)

if __name__ == '__main__':
    while True:
        try:
            print("Welcome, mere mortal, to the Fibonnaci Sequence calculator."
                  " I will be guiding you down the hallowed spiral that sits at the"
                  " heart of the natural world."
                  " Please, enter your index so that we may begin.")
         
            idx = input('Index: ')
            print("Index: " + idx)
         
            CondVar = idx.isdigit() 
         
            if CondVar == 1:
                idx = int(idx)
         
                if idx > 100000:
                    print('Too close to the sun, Icarus; this number is not' 
                           ' meant for mortal eyes.'
                           ' Retry your index at a value less than 1e5. \r\n') 
                 
                elif idx <= 100000:
                    print('Fibonacci number at'
                          ' index {:} is {:}.\r\n'.format(idx,fib(idx)))
                         
                    
         
            elif CondVar != 1:
                print('To probe the depths of the golden ratio, refrain' 
                      ' from inserting strings or decimal values.\r\n')
                # carriage return charcters and a new line
        
        except KeyboardInterrupt:
            break    
        
    print('... And now a Fibonacci number will follow you home, HAHAHAHAHAHA.')
        
    
   
    
    



