# -*- coding: utf-8 -*-
"""@file    HW2_ElevFSM.py
@brief      Simulates an elevator traveling between 2 floors
@details    Implements a finite state machine, shown below,...
            to simulate the behavior of an elevator in a 2-story building
@author     Nicholas Greco
@date       1/27/2021
"""

import time
import random

def elev_cmd(cmd): # simulating elevator operations
    '''@brief Commands the elevator motor to move up, down, or stop
       @param cmd the command to give the elevator motor
    '''
    
    if cmd == 0:
        print('Elevator STOP')
    elif cmd == 1:
        print('Elevator UP')
    elif cmd == 2:
        print('Elevator DOWN')
        
def button_1():
    '''@brief  Defines whether or not button 1 is active
       @return Randomly select between True or False
    '''
    return random.choice([True,False]) 
    # if lit == 0:
    #     print('First floor light OFF')
    # elif lit == 1:
    #     print('First floor light ON')

def button_2():
    '''@brief  Defines whether or not button 2 is active
       @return Randomly select between True or False
    '''
    return random.choice([True,False]) 
    # if lit == 0:
    #     print('Second floor light OFF')
    # elif lit == 1:
    #     print('Second floor light ON')

def floor_1():
    '''@brief  Defines whether or not the elevator is at Floor 1
       @return Randomly select between True or False
    '''
    return random.choice([True,False]) 

def floor_2():
    '''@brief  Defines whether or not the elevator is at Floor 2
       @return Randomly select between True or False
    '''
    return random.choice([True,False]) 
    

# Main program / test program begins
# This code only runs if the scrpt is executed as main by pressing play
# but does not run if the script is imported as a module
if __name__ == "__main__":
    # No program initialization needed   
    state = 0
    ## @brief Designates the current state the elevator is in
    #  @details The means of sending the elevator between the first and 
    #           second floor. 
    
    while True:
        try:
            # main program code goes here
            button_1_light = 0
                ## @brief Indicates elevator is on Floor 1
                #  @details The variable button_1_light is set to True only 
                #           when the elevator button is "pressed" and the state
                #           is set to floor 1
            button_2_light = 0
                ## @brief Indicates elevator is on Floor 2
                #  @details The variable button_2_light is set to True only 
                #           when the elevator button is "pressed" and the state
                #           is set to floor 2
            elev_cmd(2)
            # elevator arrrives at bottom floor
            if state == 0:
                # Run state 0 (moving downward) code
                print('S0')
                if floor_1(): 
                    elev_cmd(0)
                    state = 1 # Updating state for next iteration
                
            elif state == 1:
            # run state 1 (Stopped on FLoor 1) code
                print('S1')
            # If we are at the Floor 1, stop the motor and turn the light off for the first floor button
                if button_1():
                    button_1_light = 0
                    print('As you are on Floor 1, I am disinclined to acquiesce to your request')
                if button_2():
                    button_2_light = 1
                    elev_cmd(1)
                    state = 2 
                
            elif state == 2:
            # run state 2 (Moving Up) code
                print('S2')
            # If button_2 is pressed, make the elevator motor move up
                if floor_2():
                    button_2_light = 0
                    elev_cmd(0)
                    state = 3 
                
            elif state == 3:
            # run state 3 (Stopped at Floor 2) code
                print('S3')
            # If the elevator has reached floor 2, turn the motor off and unlight the second floor button
                if button_2():
                        button_2_light = 0
                        print('As you are on Floor 2, I am disinclined to acquiesce to your request')    
                if button_1():
                    button_1_light = 1
                    elev_cmd(2)
                    state = 0
                    
            else:
                pass
                # code to run if state number is invalid
                # program should ideally nevevr reach here
                
            # Slow down execution of FSM so we can see output in console
            time.sleep(0.5)
            
        except KeyboardInterrupt:            
            # This except block catches "Ctrl-C" from the keyboard to end the            
            # while(True) loop when desired            
            print('Ctrl-c has been pressed')            
            break
                