# -*- coding: utf-8 -*-
"""
Created on Mon May 24 14:43:54 2021

@author: nicho
"""

import pyb
from ME405_FP_Encoder import ME405_FP_Encoder
import utime
from array import array

# Queue: Buffer or collection to be written and distilled 
#   Ex: the string of user inputs in the Simon Says game
# Share: Single value at any one time
#   Ex: incremental positional data for the encoder

def task2_fun ():
    """ @brief   Demonstration task which prints weird messages.
        @details This function implements Task 2, a task which is somewhat
                 sillier than Task 1 in that Task 2 won't shut up. Also, 
                 one can test the relative speed of Python string manipulation
                 with memory allocation (slow) @a vs. that of manipulation of 
                 bytes in pre-allocated memory (faster).
    """
    tim4 = pyb.Timer(4, period = 0xffff, prescaler = 0) 
    
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    
    enc = ME405_FP_Encoder(tim4, pinB6, pinB7)
    time_init = utime.ticks_ms()
    t_sample = 50 # sampling period in milliseconds; not the 492 calculated
    pos_vec = array('f', []) # switch to arrays 
    del_pos = array('f', [])
    time_vec = [time_init]
    
    while True:
        # Choose True or False below to select which method to try
        Pos = enc.update()
        print("Position: {}".format(Pos))
        pos_vec.append(Pos)
        
        Del = enc.getDelta()
        print("Delta: {}".format(Del))
        del_pos.append(Del)
        
        time_init = utime.ticks_ms()
        print("Initial_Time: {}".format(time_init))
        time_vec.append(time_init)
        
        Vel = Del/t_sample # values in ticks/ms
        # If not explicit FSM, use yield(0) to indicate to the scheduler that one iteration has passed
        yield (0) 
    

