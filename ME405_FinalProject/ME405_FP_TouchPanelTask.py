# -*- coding: utf-8 -*-
"""
Created on Tue May 25 20:38:02 2021

@author: nicho
"""
import pyb
import utime
from ME405_FP_ResTP import ResTP

def task1_fun ():
    pinA0 = pyb.Pin(pyb.Pin.cpu.A0) # ym of the touch panel
    pinA1 = pyb.Pin(pyb.Pin.cpu.A1) # xm of the touch panel
    pinA6 = pyb.Pin(pyb.Pin.cpu.A6) # yp of the touch panel
    pinA7 = pyb.Pin(pyb.Pin.cpu.A7) # xp of the touch panel

    tchPanel = ResTP(pinA0, pinA1, pinA6, pinA7)
    
    while True:
        scanRdg = tchPanel.runScan()
        startTime = utime.ticks_us()
        for _ in range(100):
            tchPanel.runScan()
            stopTime = utime.ticks_us()
        
        timePerRun = (stopTime - startTime)/100
        print(timePerRun)   
        
        print("xScanVal: {}, yScanVal: {}, zScanVal: {}".format(scanRdg[0], scanRdg[1], scanRdg[2]))
        
        yield(0)