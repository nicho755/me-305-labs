# -*- coding: utf-8 -*-
"""
Created on Tue May 18 13:59:06 2021

@author: nicho
"""

# configuring pins
import pyb
import utime
from pyb import ADC
from array import array


class ResTP:
    '''@brief Runs operations and returns values specific to the encoder count
       @details Class for updating, getting, setting, and computing the change 
                in the encoder count.
    '''
    pin0 = None 
    pin1 = None
    pin6 = None
    pin7 = None
    
    l_p = 0 
    w_p = 0
    cntr_p = [0, 0] # [x-pos, y-pos]
    splyVlt = 3.3
    
    def __init__(self, pin0, pin1, pin6, pin7):
        self.pin0 = pin0   # The y_m pin
        self.pin1 = pin1   # The x_m pin
        self.pin6 = pin6   # The y_p pin
        self.pin7 = pin7   # The x_p pin
        
        self.l_p = 176  # units of mm
        self.w_p = 100 # units of mm

        # confirm where the touch panel considers zero to be
        self.cntr_p = [self.l_p*(1/2), self.w_p*(1/2)] 
        
        # Be sure to set pin A0 (the "y_m" pin) as the pin to collect ADC data
        ## @brief Sets the pin to ADC mode and stores the values in the variable "adc"
        self.adc_XZ = pyb.ADC(self.pin0) # instantiatiing the ADC class with an object
        self.adc_Y = pyb.ADC(self.pin1)
        
        # Calibration results
        self.x_calib = -1.4
        self.y_calib = -3.5
        
        self.x_scale = 1.2
        self.y_scale = 1.3
        
    def x_scan(self):
        self.pin7.init(mode = pyb.Pin.OUT_PP, value = 1) # Setting pin associated with x_p high
        self.pin1.init(mode = pyb.Pin.OUT_PP, value = 0) # Setting pin associated with x_m low
        self.pin6.init(mode = pyb.Pin.IN) # Float yp
        
        self.adc_XZ = pyb.ADC(self.pin0) # setting y_m as ADC pin
        # Read ADC value from pin0 
        Vx_cnts = self.adc_XZ.read()
        # print(Vx_cnts)
        # Convert "x"-position voltage to counts
        Vx_volts = (Vx_cnts/4095)*self.splyVlt
        # Formula to turn Voltage due to pressure and system parameters into counts
        x_pos = ((Vx_volts/self.splyVlt)*self.l_p) - (self.cntr_p[0] + self.x_calib)
        return x_pos*self.x_scale 
    # Tune the x_scan (not properly working)
        
        
    def y_scan(self):
        self.pin6.init(mode = pyb.Pin.OUT_PP, value = 0) # Setting pin associated with y_p high
        self.pin0.init(mode = pyb.Pin.OUT_PP, value = 1) # Setting pin associated with y_m low
        self.pin7.init(mode = pyb.Pin.IN) # Float xp
        self.pin1.init(mode = pyb.Pin.IN) # Float xm
        
        # Must be included in the code because of possible ADC bug; cannot just instantiate
        # object in the constructor
        self.adc_Y = pyb.ADC(self.pin1) # set x_m as ADC pin
        Vy_cnts = self.adc_Y.read()
        # print(Vy_cnts)
        # Convert "x"-position voltage to counts
        Vy_volts = (Vy_cnts/4095)*self.splyVlt
        # Formula to turn Voltage due to pressure and system parameters into counts
        y_pos = ((Vy_volts/self.splyVlt)*self.w_p) - (self.cntr_p[1] + self.y_calib)
        return y_pos*self.y_scale
        
    
    def z_scan(self):
        self.pin6.init(mode = pyb.Pin.OUT_PP, value = 1) # Setting pin associated with y_p high
        self.pin1.init(mode = pyb.Pin.OUT_PP, value = 0) # Setting pin associated with x_m low
        self.pin7.init(mode = pyb.Pin.IN) # Float xp
        
        # can't do pin.OUT_PP without using the init function
        # self.pin6([True]) 
        # self.pin1([False])
        
        # Be sure to set pin A0 (the "y_m" pin) as the pin to collect ADC data
        ## @brief Sets the pin to ADC mode and stores the values in the variable "adc"
        
        self.adc_XZ = pyb.ADC(self.pin0) # setting y_m as ADC pin
        
        cnt_Val = self.adc_XZ.read() # reading from the ADC object
        return cnt_Val < 0.90*4095 # returns True when pressed
    
    def runScan(self):
        # assuming scan will read left to right, just as it would top down
        Tuple_ScanVal = (self.x_scan(), self.y_scan(), self.z_scan())
        return Tuple_ScanVal
 
 
if __name__ == "__main__":
    pinA0 = pyb.Pin(pyb.Pin.cpu.A0) # ym of the touch panel
    pinA1 = pyb.Pin(pyb.Pin.cpu.A1) # xm of the touch panel
    pinA6 = pyb.Pin(pyb.Pin.cpu.A6) # yp of the touch panel
    pinA7 = pyb.Pin(pyb.Pin.cpu.A7) # xp of the touch panel


    tchPanel = ResTP(pinA0, pinA1, pinA6, pinA7)
    while True:
        scanRdg = tchPanel.runScan()
        startTime = utime.ticks_us()
        for _ in range(100):
            tchPanel.runScan()
            stopTime = utime.ticks_us()
        
        timePerRun = (stopTime - startTime)/100
        print(timePerRun)   
        
        
       # xScanVal = tchPanel.x_scan()
       # yScanVal = tchPanel.y_scan()
       # zScanVal = tchPanel.z_scan()
        print("xScanVal: {}, yScanVal: {}, zScanVal: {}".format(scanRdg[0], scanRdg[1], scanRdg[2]))
        