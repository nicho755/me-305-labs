# -*- coding: utf-8 -*-
"""@file    Lab0x02_MotherBlinkinLEDs.py

@brief      A user interactive program that toggles an LED between 3 patterns
@details    Implements a finite state machine, shown below,...
            to operate a square, sine, and sawtooth wave pattern as indicated 
            by the press of a button on an external microprocessor
            A hardcopy finite state machine is used to show these transitions
            graphically. An initialization state presents the welcome message, 
            then sets the LED to lowand the state to zero. Once the transition 
            is toggled, the loop of the FSM is entered, and the states 
            progress as outlined above
            
@image      html ME305_Lab0x02_FSM.jpg
            IMAGE_PATH = C:/Users/nicho/Desktop/ME 305/ME305_Lab/Doxygen_StagingArea/Images2upload

@author     Nicholas Greco
@date       1/30/2021
"""

import math
import utime
import pyb

def ButtonPusher(IRQ_src):
    '''@brief The callback func. assoc. with the falling edge caused by the button push
       @param IRQ_src the interrupt cmd that activates when the button is pushed
       @return ButtonFlag becomes True
    '''
    
    global ButtonFlag
    ButtonFlag = True

if __name__ == '__main__':
    
    print("Welcome, dear user, to an immersive light show experience."
          " Available to you are 3 patterns:"
          " a square wave, a sine wave, and a sawtooth wave. Upon pressing"
          " the blue button (B1) on your Nucleo board, you enter the square" 
          " wave pattern, and will remain there until you press the blue"
          " button again, which will take you to the next waveform. Only one" 
          " waveform will be present at a time. Press Ctrl + C at any time to" 
          " exit the light show.") 
    
    ButtonFlag = False
    ## @brief Defines the name the program will use for a pin on the Nucleo
    #  @details Using the pyb module to access a pin at location A5 on the 
    #           Nucleo's CPU, this location is then set to a variable named
    #           pinA5 so that a software input can directly cause a hardware
    #           output.
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    ## @brief Creates a timer object at Timer 2 that triggers at 20 kHz
    #  @details This timer can be used so that the PWM waveform will only 
    #           operate for frequencies above 20000 Hz
    tim2 = pyb.Timer(2, freq = 20000)
    ## @brief Sets up a PWM controller object
    #  @details A channel to a specific timer is created. This channel is set 
    #           to PWM mode, and the channel is linked to pin A5
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    ## @brief Defines a name for the pin linked to the button B1 on the Nucleo
    #  @details Using the pyb module to access a pin at location C13 on the 
    #           Nucleo's CPU, this location is then set to a variable named
    #           pinC13 so that software and hardware can directly interface
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    ## @brief Defines the conditions for the external interrupt 
    #  @details Detects the presence of a falling edge at pin C13 and activates
    #           the callback function in response. As pin C13 is the pin linked 
    #           to the button B1, the callback is directly tied to the button 
    #           push.
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                pull=pyb.Pin.PULL_NONE, callback=ButtonPusher)
    ## @brief Takes users b/w the states of the FSM
    state = 0
    
    ## @brief A variable with a numeric value used in a state 1 time interval
    #  @details This variable is used to store the starting time of the
    #           waveform in State 1 so that the program can produce the desired 
    #           pattern. It is also used to compare with the time associated 
    #           with a button push to establish a transition between states.
    time1 = 0

    ## @brief A variable with a numeric value used in a state 2 time interval
    #  @details This variable is used to store the starting time of the
    #           waveform in State 2 so that the program can produce the desired 
    #           pattern. It is also used to compare with the time associated 
    #           with a button push to establish a transition between states.
    time2 = 0
 
    ## @brief A variable with a numeric value used in a state 3 time interval
    #  @details This variable is used to store the starting time of the
    #           waveform in State 3 so that the program can produce the desired 
    #           pattern. It is also used to compare with the time associated 
    #           with a button push to establish a transition between states.
    time3 = 0
    
    
    # This is the initializaiton state
    
    # Your program must function without error even when the user behaves 
    # counter to the instructions. For instance, if the user pushes the button 
    # too fast, holds it down, or never pushes it, your program must behave 
    # gracefully and handle those inputs without halting execution.
    
    while(True): 
        try:
            #introduce step wave pattern
            if state == 0:
                # pinA5.high()/low() DOESN'T WORK
                t2ch1.pulse_width_percent(0)
                
                if ButtonFlag == True:
                    state = 1
                    ButtonFlag = False
                    time1 = utime.ticks_ms()
                    # print(time1) - originially for troubleshooting purposes,
                    # now a comment of a good practice I need to carry forward
                    print('Entering State 1, the Square Wave, from Init')
                
            elif state == 1:
                # when button B2, whose output directly affects the state of 
                # PC13, is pressed PC13 will change from high to low. This 
                # "falling edge" will indicate to the Nucleo that the button 
                # has been pressed.
                
                # diff = (utime.ticks_ms() - time1) % 1
                if utime.ticks_diff(utime.ticks_ms(), time1) % 1000 < 500:
                    t2ch1.pulse_width_percent(100)
                elif utime.ticks_diff(utime.ticks_ms(), time1)  >= 500:
                    t2ch1.pulse_width_percent(0)
                    
                if ButtonFlag == True:
                    state = 2
                    ButtonFlag = False
                    time2 = utime.ticks_ms()
                    print('Entering State 2, the Sine Wave')
                
            # Now, we need to toggle the sine wave function by activating PC13 again

            elif state == 2:
                ## @brief Returns the time diff. between a counter and a fixed reference
                #  @details A fixed time value established by the button push  
                #           in the transition prior to this state is subtracted
                #           from a function that serves as a counter constantly
                #           updating in value since the last button push. This 
                #           expression is then moded with 10000ms to create a
                #           defined period that will cause the waveform to 
                #           repeat its behavior at 10s (10000ms) intervals.
                diff1 = (utime.ticks_ms() - time2) % 10000
                ## @brief Establishes the waveform of the second state
                #  @details Takes the value of diff1 above and alters the 
                #           waveform based upon the time value returned in 
                #           diff1. The function ensures that the LED starts at
                #           50% power at the beginning of the cycle, and that
                #           the value will follow a sine wave pattern.
                oscPct = math.sin(diff1/5000 * math.pi) * 50 + 50
                t2ch1.pulse_width_percent(oscPct)
                
                if ButtonFlag == True:
                    state = 3
                    ButtonFlag = False
                    time3 = utime.ticks_ms()
                    print('Entering State 3, the Sawtooth Wave')
                    
            elif state == 3:
                ## @brief Converts a moded time interval in ms to percent
                #  @details Establishes a time interval using a fixed reference
                #           value established in the transition to this state, 
                #           and compares it with a counter that is continually 
                #           increasing. This value is then moded to set the
                #           waveform's period. Finally, the result is divided 
                #           by 10 in order to get a percentage that can then be
                #           plugeed into a PWM signal.
                pct = ((utime.ticks_ms() - time3) % 1000)/10    
                # Because pct returns results in ms and I need to convert to 
                # percent for the command below, I need to convert from ms to 
                # percentage. I will do this by diividing the pct value by 10
                t2ch1.pulse_width_percent(pct)
                
                if ButtonFlag == True:
                    state = 1
                    ButtonFlag = False
                    time1 = utime.ticks_ms()
                    # same name as above because this variable needs to be 
                    # called by the if-else statement in state 1 in order to
                    # operate the square wave.
                    # I think naming the variable the same name will just 
                    # overwrite the previous value (which I am ok with)
                    print('Re-entering State 1, the Square Wave')
            
        except KeyboardInterrupt:
            # THis except block catches Ctrl-C from the keyboard
            # and only broke the while(True) loop when desired
            t2ch1.pulse_width_percent(0)
            break
            
    #Program De-Initialization goes here
    print ('Sun is setting high')    

